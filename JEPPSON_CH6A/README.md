# README #

`JEPPSON_CH6A` is a Newton-Raphson method piping network flow solver based on the Newton-Raphson
solver code described on pages 63 of (Jeppson, 1974) and pages 119-121 of
(Jeppson, 1976).

The proprietary UNIVAC MATH-PACK library `GJR` has been reverse-engineered and
replaced with a compatibility routine based on the widely-available LAPACK
linear algebra library. Proprietary UNIVAC "FORTRAN 5" features were also
removed (primarily the alternate return point in the call to GJR()).

Build instructions have been provided for Linux using both gfortran and the
Intel ifort compiler. The code and documentation can be built using the CMake
cross-platform build system. A simple Makefile is provided in the `src`
directory for building the JEPPSON_CH6A executable if CMake is not available.

Several test cases are provided under the `test` directory which give the input
and output for the example case given in the text. Note that the diagrams for
examples 2 and 3 in the text are incorrect, incomplete, and inconsistent with
the calculations, and the example 3 results are wrong due to miscalculation of
the K factor between nodes 6 and 7 (for CHW = 120, K67 = 3.243, not 29.27).

## License ##

No license was provided with the original source code as published in either
(Jeppson, 1974) paper or (Jeppson, 1976). All other code provided under the MIT
(`expat`) license as written in `LICENSE.md`

## Who do I talk to? ##

Questions or comments can be directed to the project maintainer,
<bob.apthorpe@gmail.com>

## References ##

* Jeppson, Roland W. *Steady Flow Analysis of Pipe Networks: An Instructional
  Manual* (1974). *Reports.* Paper 300.
  http://digitalcommons.usu.edu/water_rep/300 
* Jeppson, Roland W., *Analysis of Flow in Pipe Networks* (1976). Ann Arbor
  Science Publishers, Inc.
  http://www.worldcat.org/title/analysis-of-flow-in-pipe-networks/oclc/927534147
