# Building the Software

## Overview

The Jeppson Newton-Raphson solver uses CMake as its build system. The general
method of building software with CMake is to unpack the source archive, create
a separate build directory outside the source tree, run CMake to populate the
build directory and configure tools, then run `make` in the build directory to
compile and link the code. The executable will be located in the `src` folder
within the build directory.

Note that if CMake is not available, `Makefile`, `build_with_gfortran.sh`, or
`build_with_ifort.sh` may be modified to build JEPPSON_CH6A from within
`src` directory in the original source archive.

## Prerequisites

 * A modern Fortran compiler (tested with gfortran 5.2.0 and Intel ifort 13.1.0
   on Linux)
 * LAPACK linear algebra library, available at http://www.netlib.org/lapack/
 * CMake (at least version 2.8.7), available at https://cmake.org/

LAPACK is often supplied as prepackaged vendor library with Linux distributions
or as part of the Intel Math Kernel Library (MKL)

To build the documentation, *doxygen* (http://www.stack.nl/~dimitri/doxygen/)
is necessary. If PDF documentation is desired, a working *LaTeX* installation
such as TeXLive (https://www.tug.org/texlive/) is required. Neither are
strictly required to build and run JEPPSON_CH6A.

## Unpack the Source Archive

Unzip this archive

    unzip -r JEPPSON_CH6A.zip

(files unzip to ./JEPPSON_CH6A)

## Create the Build Environment

Create a build directory and move to it:

    mkdir ./build
    cd ./build

Execute cmake setting the specific fortran compiler with
`CMAKE_Fortran_COMPILER` (`gfortran` or `ifort`) and setting the build type
with `CMAKE_BUILD_TYPE` (`DEBUG` or `RELEASE`) Remember to specify the base
directory of this archive as an argument (`../JEPPSON_CH6A`)

    cmake -D CMAKE_Fortran_COMPILER=/home/apthorpe/bin/gcc/5.2.0/bin/gfortran \
          -D CMAKE_BUILD_TYPE=RELEASE  ../JEPPSON_CH6A 

## Build the Software and Documentation

Build the software using `make` or equivalent build tool

    make

The executable is located at `./src/JEPPSON_CH6A`

Build the documentation with

    make doc

To build the documentation directly, run

    doxygen Doxyfile

from the root of the original archive directory.

To generate PDF documentation via *LaTeX*,

    cd ./doc/latex 
    make 
    cd ../..

and the results will be in `./doc/latex/refman.pdf` beneath the build
directory.

## Test the Software

Test cases and reference data are located in the source archive under
`JEPPSON_CH6A/test`. The case in `JEPPSON_CH6A/test/test1` is based on the
example preceeding the source code in Chapter 6 of Jeppson's text. Run the case
with

    ./src/JEPPSON_CH6A < ../JEPPSON_CH6A/test/test1/test1.inp | tee test1.out

Compare to the reference results with

    diff test1.out ../JEPPSON_CH6A/test/test1/ref/test1.out

These results are consistent with the flow results reported in the text.

## To Do

This process should be expanded to include setting up a generator for Visual
Studio and the Intel compiler on Windows.
