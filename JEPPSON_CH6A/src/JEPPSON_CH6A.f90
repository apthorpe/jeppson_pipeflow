!> Newton-Raphson pipe flow solver
      PROGRAM JEPPSON_CH6A
!
      use alfc_sperry_mathpack, only: GJR
!
      INTEGER N1(50),N2(50),NN(45),JB(45,7),JC(45)
      REAL H(45),D(50),L(50),Q(50),CHW(50),QJ(45),K(45),F(45,46),V(2)
      CONTINUE
! NP-NO. PIPES, NJ-NO. OF JUNCTIONS, KNOWN-
! JUNCTION NO. OF KNOWN HEAD, MAXITER-MAX. NO. OF
! JUNCTIONS ALLOWED, ERR-ERROR PARAMETER
98    CONTINUE
      READ(5,100,END=99) NP,NJ,KNOWN,MAXITER,ERR
!      write(6,"('CARD ', I3, ': NP = ', I3, ', NJ = ', I3, "            &
!           // "', KNOWN = ', I3, ', MAXITER = ', I3, ', ERR = ',"       &
!           // "ES12.4)") 1, NP, NJ, KNOWN, MAXITER, ERR
! 100   FORMAT(4I5,4F10.5)
100   FORMAT(4I5,F10.5)
      DO 2 I=1,NP
! N1 (I) JUNCTION NO. FROM WHICH FLOW IN PIPE COMES
! N2(I) JUNCTION NO. TO WHICH FLOW IN PIPE GOES
! D(I)-DIAMETER OF PIPE IN INCHES
! CHW(I)-HAZEN-WILLIAMS COEFFICIENT FOR PIPE
! L--LENGTH OF PIPE IN FEET
      READ(5,101) N1(I),N2(I),D(I),CHW(I),L(I)
      D(I)=D(I)/12.
      K(I)=4.727328*L(I)/(CHW(I)**1.85185185*D(I)**4.87037)
!      write(6,"('CARD ', I3, ': I = ', I3, ', N1 = ', I3, "             &
!           // "', N2 = ', I3, ', D = ', ES12.4, "                       &
!           // "', CHW = ', ES12.4, ', L = ', ES12.4, ', k', ES12.4)")   &
!           I+1, I, N1(I), N2(I), 12.0*D(I), CHW(I), L(I), K(I)
2     CONTINUE
101   FORMAT(2I5,3F10.5)
      NJM=NJ-1
      DO 4 J=1,NJ
! I-JUNCTION NO.
! QJ(I)-EXTERNAL FLOW AT JUNCTION, MINUS IF OUT
! FROM NETWORK
! H(I)-ESTIMATE OF HEAD AT JUNCTION USED TO
! INITIALIZE N-R SOLUTION
      READ(5,102) I,QJ(I),H(I)
!      write(6,"('CARD ', I3, ': I = ', I3, ', QJ = ', ES12.4,"          &
!           // "', H = ', ES12.4)") NP+J+1, I, QJ(I), H(I)
4     CONTINUE
! 102   FORMAT(I5,5F10.5)
102   FORMAT(I5,2F10.5)
      DO 5 J=1,NJ
      NNP=0
      DO 6 I=1,NP
      IF(N1(I) .NE. J) GO TO 7
      NNP=NNP+1
      JB(J ,NNP)=I
      GOTO 6
7     CONTINUE
      IF(N2(I) .NE. J) GO TO 6
      NNP=NNP+1
      JB(J,NNP)=-I
6     CONTINUE
      NN(J)=NNP
5     CONTINUE
      NCT=0
20    CONTINUE
      SSUM=0.
      JE=0
      DO 10 J=1,NJ
      IF(J .EQ. KNOWN) GO TO 10
      JE=JE+1
      JJE=J-JE
      DO 15 JJ=1,NJ
      F(JE,JJ)=0.
15    CONTINUE
      NNP=NN(J)
      DO 11 KK=1,NNP
      II=JB(J,KK)
      I=ABS(II)
      I1=N1(I)
      I2=N2(I)
      ARG=(H(I1)-H(I2))/K(I)
      FAC=II/I
      FAC5=.54*FAC
!      if (ARG .LT. 0.0) then
!          write(6,"('H(',I3,') - H(',I3,'))/K(',I3,') = (', ES12.4,"    &
!               // "' - ', ES12.4, ') / ', ES12.4, ' = ', ES12.4)")      &
!              I1, I2, I, H(I1), H(I2), K(I), ARG
!      end if
      ARGE=ARG**.54
!      write(6,"('Z(', I3,') = ', ES12.4, ' + ', ES12.4, ' = ',"         &
!           // " ES12.4)") JE, F(JE,NJ), ARGE*FAC, F(JE,NJ)+ARGE*FAC
! 13    F(JE,NJ)=F(JE,NJ)+ARGE*FAC
      F(JE,NJ)=F(JE,NJ)+ARGE*FAC
      IF(I1 .EQ. KNOWN) GO TO 14
      IF(I1 .GT. KNOWN) I1=I1-1
      F(JE,I1)=F(JE,I1)+FAC5* ARGE/(K(I)* ARG)
14    CONTINUE
      IF(I2 .EQ. KNOWN) GO TO 11
      IF(I2 .GT. KNOWN) I2=I2-1
      F(JE,I2)=F(JE,I2)-FAC5 * ARGE/(K(I)* ARG)
11    CONTINUE
!      write(6,"('Z(', I3,') = ', ES12.4, ' - ', ES12.4, ' = ',"         &
!           // " ES12.4)") JE, F(JE,NJ), QJ(J), F(JE,NJ)-QJ(J)
      F(JE,NJ)=F(JE,NJ)-QJ(J)
10    CONTINUE
      V(1)=4.
! System subroutine from UNIVAC MATH-PACK to solve linear system of
! equations
! Sperry alternate return point syntax not supported
! It appears that NJM is misspelled as 'N, JM'
!      CALL GJR(F,46,45,N,JM,NJ ,$97 ,JC,V)
!      write(6, "(/, 'Iteration ', I4, ': Solving...')") NCT+1
!      DO IDX = 1, NJM
!          WRITE(6,"(46(ES12.4, :, 4X))") (F(IDX, JDX), JDX=1,NJ)
!      END DO
      CALL GJR(F,46,45,NJM,NJ,97,JC,V)
      IF (JC(1) .NE. NJM) THEN
          WRITE(6, "('Matrix failure:', /,"                             &
              // "'JC(1): ', I3, ' <> NJM: ', I3)") JC(1), NJM
          GOTO 97
      END IF
!      WRITE(6,"('Z = ', 46(ES12.4, :, 4X))") (F(IDX, NJ), IDX=1,NJM)
      JE=0
      DO 24 J=1,NJ
      IF(J .EQ. KNOWN) GO TO 24
      JE=JE+1
      DIF=F(JE,NJ)
      SSUM=SSUM+ABS(DIF)
      H(J)=H(J)-DIF
24    CONTINUE
      DO 25 I=1,NP
      I1=N1(I)
      I2=N2(I)
      IF(H(I1) .GT. H(I2)) GO TO 25
      WRITE(6,225) I,I1,I2
225   FORMAT(' FLOW HAS REVERSED IN PIPE',3I5)
      N1(I)=I2
      N2(I)=I1
      II=I1
28    CONTINUE
      NNP=NN(II)
      DO 26 KK=1,NNP
      IF(ABS(JB(II,KK)) .NE. I) GO TO 26
      JB(II,KK)=-JB(II,KK)
      GO TO 27
26    CONTINUE
27    CONTINUE
      IF(II .EQ. I2) GO TO 25
      II=I2
      GO TO 28
25    CONTINUE
      NCT=NCT+1
      WRITE(6,108) NCT,SSUM
108   FORMAT(' NCT=',I5,' SUM=',E12.5)
      IF(NCT .LT. MAXITER .AND. SSUM .GT. ERR) GO TO 20
      WRITE(6, 103)(H(J),J = 1,NJ)
103   FORMAT(' HEADS AT JUNCTIONS',/,(1X, 13F10.3))
      WRITE(6,104)
104   FORMAT(' FROM   TO  DIAMETER    LENGTH',                           &
             '      CHW   FLOWRATE HEAD LOSS  HEADS AT JUNCTIONS')
      DO 17 I=1,NP
      I1=N1(I)
      I2=N2(I)
      DH=H(I1)-H(I2)
      Q(I)=(DH/K(I))**.54
! 19    WRITE(6,105) I1,I2,D(I),L(I),CHW(I),Q(I),DH,H(I1),H(I2)
      WRITE(6,105) I1,I2,D(I),L(I),CHW(I),Q(I),DH,H(I1),H(I2)
105   FORMAT(2I5,2F10.1,F10.0,4F10.3)
17    CONTINUE
      GO TO 98
97    CONTINUE
      WRITE(6,306) JC(1), V
306   FORMAT(' OVERFLOW OCCURRED-CHECK SPEC. FOR REDUNDANT EO.'         &
             // ' RESULTING IN SINGULAR MATRIX', I5, 2F10.2)
99    STOP
      END PROGRAM JEPPSON_CH6A
