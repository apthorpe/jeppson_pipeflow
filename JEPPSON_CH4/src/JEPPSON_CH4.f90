!> Incompressible flow pipe network solver
      PROGRAM JEPPSON_CH4
100   FORMAT(8F10.5)
      ELOG=9.35*LOG10(2.71828183)
20    READ(5,100,END=99) Q,D,FL,E,DQP,VIS
      DEQ=Q*DQP
      ED=E/D
      D=D/12.
      A=.78539392 *D**2
      Q1=Q-DEQ
      Q2=Q+DEQ
      V1=Q1/A
      V2=Q2/A
      RE1=V1*D/VIS
      RE2=V2*D/VIS
      ARL=FL/(64.4*D*A**2)
      F=1./(1.14-2.*LOG10(ED))**2
      RE=RE1
      MM=0
57    CONTINUE
      MCT=0
52    CONTINUE
      FS=SQRT(F)
      FZ=.5/(F*FS)
      ARG=ED+9.35/(RE*FS)
      FF=1./FS-1.14+2.*LOG10(ARG)
      DF=FZ+ELOG*FZ/(ARG*RE)
      DIF=FF/DF
      F=F+DIF
      MCT=MCT+1
      IF(ABS(DIF) .GT. .00001 .AND. MCT .LT. 15) GO TO 52
      IF(MM .EQ. 1) GO TO 55
      MM=1
      RE=RE2
      F1=F
      GO TO 57
55    F2=F
      BE=(LOG(F1)-LOG(F2))/(LOG(Q2)-LOG(Q1))
      AE=F1 * (Q-DEQ)**BE
      EP=2.-BE
      CK=AE*ARL
      WRITE(6,101) Q,D,BE,AE,EP,CK
101   FORMAT(1X,5F12.5,E16.6)
      GO TO 20
99    STOP
      END PROGRAM JEPPSON_CH4
