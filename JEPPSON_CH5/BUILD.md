# Building the Software

## Overview

The Jeppson linear method solver uses CMake as its build system. The general
method of building software with CMake is to unpack the source archive, create
a separate build directory outside the source tree, run CMake to populate the
build directory and configure tools, then run `make` in the build directory to
compile and link the code. The executable will be located in the `src` folder
within the build directory.

Note that if CMake is not available, Makefile, `build_with_gfortran.sh`, or
`build_with_ifort.sh` may be modified to build JEPPSON_CH5 from within
`src` directory in the original source archive.

## Prerequisites

 * A modern Fortran compiler (tested with gfortran 5.2.0 and Intel ifort 13.1.0
   on Linux)
 * LAPACK linear algebra library, available at http://www.netlib.org/lapack/
 * CMake (at least version 2.8.7), available at https://cmake.org/

LAPACK is often supplied as prepackaged vendor library with Linux distributions
or as part of the Intel Math Kernel Library (MKL)

To build the documentation, *doxygen* (http://www.stack.nl/~dimitri/doxygen/)
is necessary. If PDF documentation is desired, a working *LaTeX* installation
such as TeXLive (https://www.tug.org/texlive/) is required. Neither are
strictly required to build and run JEPPSON_CH5.

## Unpack the Source Archive

Unzip this archive

    unzip -r JEPPSON_CH5.zip

(files unzip to ./JEPPSON_CH5)

## Create the Build Environment

Create a build directory and move to it:

    mkdir ./build
    cd ./build

Execute cmake setting the specific fortran compiler with
`CMAKE_Fortran_COMPILER` (`gfortran` or `ifort`) and setting the build type
with `CMAKE_BUILD_TYPE` (`DEBUG` or `RELEASE`) Remember to specify the base
directory of this archive as an argument (`../JEPPSON_CH5`)

    cmake -D CMAKE_Fortran_COMPILER=/home/apthorpe/bin/gcc/5.2.0/bin/gfortran \
          -D CMAKE_BUILD_TYPE=RELEASE  ../JEPPSON_CH5 

## Build the Software and Documentation

Build the software using `make` or equivalent build tool

    make

The executable is located at `./src/JEPPSON_CH5`

Build the documentation with

    make doc

To build the documentation directly, run

    doxygen Doxyfile

from the root of the original archive directory.

To generate PDF documentation via *LaTeX*,

    cd ./doc/latex 
    make 
    cd ../..

and the results will be in `./doc/latex/refman.pdf` beneath the build
directory.

## Test the Software

Test cases and reference data are located in the source archive under
`JEPPSON_CH5/test`. The case `test1.inp` is based on the example preceeding the
source code in Chapter 5 of Jeppson's text. Run the case with

    ./src/JEPPSON_CH5 < ../JEPPSON_CH5/test/test1.inp | tee test1.out

Compare to the reference results with

    diff test1.out ../JEPPSON_CH5/test/ref/test1.out

These results are consistent with the flow results reported in the text.

The test2 case corrects the test1 case to match the stated condiitons in the
text where pipe 2 is 750.0 feet long with a flow obstruction equivalent to 51.0
feet of pipe, leading to a pipe 2 effective length of 801.0 (test1 incorrectly
takes this length as 751.0 feet; the results reported in the text are
consistent with this error).

The change in results is small as expected for increased frictional losses and
the flows and pressure drops are consistent with the change to the system.

## To Do

This process should be expanded to include setting up a generator for Visual
Studio and the Intel compiler on Windows.
