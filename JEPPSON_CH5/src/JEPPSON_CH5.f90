      PROGRAM JEPPSON_CH5
!
      use alfc_sperry_mathpack, only: GJR
!      use diag, only: show_mat
!
      INTEGER, dimension(40,5) :: JN
      INTEGER, dimension(40) :: NN
      INTEGER, dimension(20) :: JB
      INTEGER, dimension(40) :: IFLOW
      INTEGER, dimension(8,20) :: LP
      INTEGER, dimension(50) :: JC
      REAL, dimension(50) :: D
      REAL, dimension(50) :: L
      REAL, dimension(50,51) :: A
      REAL, dimension(20) :: QJ
      REAL, dimension(50) :: E
      REAL, dimension(50) :: KP
      REAL, dimension(2) :: V
      REAL, dimension(50) :: Q
      REAL, dimension(50) :: EXPP
      REAL, dimension(50) :: AR
      REAL, dimension(50) :: ARL
      CONTINUE
! Initialize integers
      JN = 0
      NN = 0
      JB = 0
      IFLOW = 0
      LP = 0
      JC = 0
! Initialize reals
      D = 0.0
      L = 0.0
      A = 0.0
      QJ = 0.0
      E = 0.0
      KP = 0.0
      V = 0.0
      Q = 0.0
      EXPP = 0.0
      AR = 0.0
      ARL = 0.0
30    CONTINUE
      READ(5, 110, END=99) NP, NJ, NL, MAXITER, NUNIT, ERR, VIS, DELQ1
110   FORMAT(5I5, 3F10.5)
! NP  - NO. OF PIPES.
! NJ  - NO. OF JUNCTIONS
! NL  - NO. OF LOOPS
! MAXITER - NO. OF ITERATIONS ALLOWED.
! IF NUNIT=O, D AND E IN INCHES AND L IN FEET
! IF NUNIT=1, D AND E IN FEET AND L IN FEET
! IF NUNIT=2, D AND E IN METERS AND L IN METERS
! IF NUNIT=3, D AND E IN CM C AND L IN METERS
100   FORMAT(16I5)
      NPP=NP+1
      NJ1=NJ-1
      READ(5,101) (D(I), I=1,NP)
      READ(5,101) (L(I), I=1,NP)
      READ(5,101) (E(I), I=1,NP)
101   FORMAT(8F10.5)
      DO 48 I = 1, NP
          E(I) = E(I) / D(I)
48    CONTINUE
      IF (NUNIT-1) 40, 41, 42
40    CONTINUE
      WRITE(6,102) (D(I), I=1,NP)
102   FORMAT('O PIPE DIAMETERS (INCHES)', /, 16(' ', F8.1))
      DO 43 I = 1, NP
          D(I) = D(I) / 12.0
43    CONTINUE
      GOTO 44
41    CONTINUE
      WRITE(6,112) (D(I), I=1,NP)
112   FORMAT('O PIPE DIAMETERS (FEET)', /, 16(' ', F8.3))
44    CONTINUE
      WRITE(6, 103) (L(I), I=1,NP)
103   FORMAT('O LENGTHS OF PIPE (FEET)', /, 16(' ',F8.0))
      G2 = 64.4
      GOTO 50
! NOTE: Label 50 not defined
42    CONTINUE
      IF (NUNIT .EQ. 2) GOTO 45
      DO 46 I = 1, NP
      D(I) = 0.01 * D(I)
46    CONTINUE
45    CONTINUE
      WRITE(6,113) (D(I), I=1,NP)
113   FORMAT('O PIPE DIAMETERS (METERS)', /, 16(' ', 16F8.4))
      WRITE(6,114) (L(I), I=1,NP)
114   FORMAT('O LENGTHS OF PIPES (METERS)', /, 16(' ',F8.1))
      G2 = 19.62
50    CONTINUE ! Assumed location of label 50
      WRITE(6,115) (E(I), I=1,NP)
115   FORMAT('O RELATIVE ROUGHNESS OF PIPES', /, 16(' ', F8.6))
! INFLOW--IF 0 NO INFLOW
!         IF 1 THEN NEXT CARD GIVES MAGNITUDE IN GPM
!         IF 2 THEN NEXT CARD GIVES MAGNITUDE IN FT**3/SEC.
!         IF 3 THEN NEXT CARD GIVES MAGNITUDE IN M**3/SEC.
! NNJ - NO. OF PIPES AT JUNCTIONS
!       - POSITIVE FOR INFLOW
!       - NEGATIVE FOR OUTFLOW.
! IN  - THE NUMBERS OF PIPES C AT JUNCTION,
!       - IF FLOW ENTERS, MINUS
!       - IF FLOW LEAVES, THE PIPE NUMBER IS POSITIVE.
      DO 70 I = 1, NP
          AR(I) = 0.78539392 * D(I)**2
          ARL(I) = L(I) / (G2 * D(I) * AR(I)**2)
70    CONTINUE
      II = 1
      DO 1 I = 1, NJ
          READ(5,100) IFLOW(I), NNJ, (JN(I,J), J=1,NNJ)
          NN(I) = NNJ
          IF (IFLOW(I) - 1) 1, 2, 3
2             CONTINUE
              READ(5, 101) QJ(II)
              QJ(II) = QJ(II) / 449.0
              JB(II) = I
              GOTO 4
3             CONTINUE
              READ(5, 101) QJ(II)
!              BJ(II) = I ! Error?
              JB(II) = I
4         CONTINUE
          II = II + 1
1     CONTINUE
! NUMBER OF PIPES IN EACH LOOP (SIGN INCLUDED)
      DO 35 I = 1, NL
          READ(5,100) NNJ, (LP(J,I), J=1,NNJ)
          LP(8,I) = NNJ
35    CONTINUE
      DO 5 I = 1,NP
      IF (NUNIT .GT. 1) GOTO 66
      KP(I) = 0.0009517 * L(I) / D(I)**4.87
      GOTO 5
66    CONTINUE
      KP(I) = 0.00212 * L(I) / D(I)**4.87
5     CONTINUE
      ELOG = 9.35 * LOG10(2.71828183)
      SSUM = 100.0
      NCT = 0
20    CONTINUE
!1100  format('Setting A(', I3, ', ', I3, ') to ', G15.8)
      II = 1
      DO 6 I = 1, NJ1
          DO 7 J = 1, NP
              A(I,J) = 0.0
!              write(6, 1100) I, J, A(I, J)
7         CONTINUE
          NNJ = NN(I)
          DO 8 J = 1, NNJ
              IJ = JN(I,J)
              IF (IJ .GT. 0) GOTO 9
              IIJ = ABS(IJ)
              A(I,IIJ) = -1.0
!              write(6, 1100) I, IIJ, A(I, IIJ)
              GOTO 8
9             CONTINUE
              A(I,IJ) = 1.0
!              write(6, 1100) I, IJ, A(I, IJ)
8         CONTINUE
          IF (IFLOW(I) .EQ. 0) GOTO 10
          A(I,NPP) = QJ(II)
!          write(6, 1100) I, NPP, A(I, NPP)
          II = II + 1
          GOTO 6
10        CONTINUE
          A(I,NPP) = 0.0
!          write(6, 1100) I, NPP, A(I, NPP)
6     CONTINUE
      DO 11 I=NJ,NP
          DO 22 J=1,NP
              A(I,J) = 0.0
!              write(6, 1100) I, J, A(I, J)
22        CONTINUE
          II = I - NJ1
          NNJ = LP(8,II)
          DO 12 J = 1, NNJ
              IJ = LP(J ,II)
              IIJ = ABS(IJ)
              IF (IJ .LT. 0) GOTO 13
              A(I, IIJ) = KP(IIJ)
!              write(6, 1100) I, IIJ, A(I, IIJ)
              GOTO 12
13            CONTINUE
              A(I, IIJ) = -KP(IIJ)
!              write(6, 1100) I, IIJ, A(I, IIJ)
12        CONTINUE
          A(I,NPP) = 0.0
!          write(6, 1100) I, NPP, A(I, NPP)
11    CONTINUE
!      write(6, *) 'Done setting A entries'
!      call show_mat(A, 'Full ')
!      write(6, *) 'Done showing A entries'
      V(1) = 4.0
! System subroutine from UNIVAC MATH-PACK to solve linear system of
! equations
! Sperry alternate return point syntax not supported
!      CALL GJR(A, 51, 50, NP, NPP, $98, JC, V)
      CALL GJR(A, 51, 50, NP, NPP, 98, JC, V)
      IF (JC(1) .NE. NP) THEN
          WRITE(6, "('Matrix failure:', /,"                             &
              // "'JC(1): ', I3, ' <> NP: ', I3)") JC(1), NP
          GOTO 98
      END IF
      IF (NCT .GT. 0) SSUM = 0.0
      DO 51 I = 1, NP
      BB = A(I,NPP)
      IF (NCT) 60, 60, 61
60    CONTINUE
      QM = BB
      GOTO 62
61    CONTINUE
      QM = 0.5 * (Q(I) + BB)
      SSUM = SSUM + ABS(Q(I) - BB)
62    CONTINUE
      Q(I) = QM
      DELQ = QM * DELQ1
      QM = ABS(QM)
      V1 = (QM - DELQ) / AR(I)
      IF (V1 .LT. 0.001) V1 = 0.002
      V2 = (QM + DELQ) / AR(I)
      VE = QM / AR(I)
      RE1 = V1 * D(I) / VIS
      RE2 = V2 * D(I) / VIS
      IF (RE2 .GT. 2.1E3) GOTO 53
      F1 = 64.0 / RE1
      F2 = 64.0 / RE2
      EXPP(I) = 1.0
      KP(I) = 64.4 * VIS * ARL(I) / D(I)
      GOTO 51
53    CONTINUE
      MM = 0
      F = 1.0 / (1.14 - 2.0 * LOG10(E(I)))**2
      PAR = VE * SQRT(0.125 * F) * D(I) * E(I) / VIS
      IF (PAR .GT. 65.0) GOTO 54
      RE = RE1
57    CONTINUE
      MCT = 0
52    CONTINUE
      FS = SQRT(F)
      FZ = 0.5 / (F * FS)
      ARG = E(I) + 9.35 / (RE * FS)
      FF= 1.0 / FS - 1.14 + 2.0 * LOG10(ARG)
      DF = FZ + ELOG * FZ / (ARG * RE)
      DIF = FF / DF
      F = F + DIF
      MCT = MCT + 1
      IF (ABS(DIF) .GT. 0.00001 .AND. MCT .LT. 15) GOTO 52
      IF (MM .EQ. 1) GOTO 55
      MM = 1
      RE = RE2
      F1 = F
      GOTO 57
55    CONTINUE
      F2 = F
      BE = (LOG(F1) - LOG(F2)) / (LOG(QM + DELQ) - LOG(QM - DELQ))
      AE = F1 * (QM - DELQ)**BE
      EP = 1.0 - BE
      EXPP(I) = EP + 1.0
      KP(I) = AE * ARL(I) * QM ** EP
      GOTO 51
54    CONTINUE
      KP(I) = F * ARL(I) * QM**2
      EXPP(I) = 2.0
51    CONTINUE
! 17    CONTINUE ! Not referenced
      NCT = NCT + 1
! THE NEXT FIVE CARDS CAN BE REMOVED
      WRITE(6,157) NCT, SSUM, (Q(I), I=1,NP)
157   FORMAT('NCT=', I3, ' SUM=', E10.3, /, (' ', 13F10.3))
      WRITE(6,344) (EXPP(I), I=1,NP)
344   FORMAT(' ', 13F10.3)
      WRITE(6,344) (KP(I), I=1,NP)
      IF (SSUM .GT. ERR .AND. NCT .LT. MAXITER) GOTO 20
      IF (NCT .EQ. MAXITER) WRITE(6,108) NCT, SSUM
108   FORMAT('DID NOT CONVERGE IN', I5, ' ITERATIONS',                  &
     & ' - SUM OF DIFFERENCES = ', E10.4)
      IF (NUNIT .LT. 2) GOTO 63
      WRITE(6,127) (Q(I), I=1,NP)
127   FORMAT('O FLOWRATES IN PIPES IN M**3/SEC', /,                     &
     & (' ', 13E10.4))
      DO 64 I = 1, NP
          KP(I) = KP(I) * ABS(Q(I))
64    CONTINUE
!      WRITE(6, 139) (KP(I), I=1,NP) ! Format label seems wrong
      WRITE(6, 138) (KP(I), I=1,NP)
      GOTO 30
63    CONTINUE
      WRITE(6, 107) (Q(I), I=1,NP)
107   FORMAT('O FLOWRATES IN PIPES IN FT**3/SEC', /,                    &
     & (' ', 13F10.3))
      DO 21 I = 1, NP
          KP(I) = KP(I) * ABS(Q(I))
          Q(I) = 449.0 * Q(I)
21    CONTINUE
      WRITE(6,138) (KP(I), I=1,NP)
138   FORMAT(' HEAD LOSSES IN PIPES'/,(' ', 13F10.3))
      WRITE(6,105) (Q(I), I=1,NP)
105   FORMAT(' FLOW RATES (GPM)', /, (' ',13F10.1))
      GOTO 30
98    CONTINUE
      WRITE(6,106) JC(1), V
106   FORMAT(' OVERFLOW OCCURRED - CHECK ',                             &
     & 'SPECIFICATIONS FOR REDUCDANT EQ. RESULTING ',                   &
     & 'IN SINGULAR MATRIX ', I5, 2F8.2)
      GOTO 30
99    CONTINUE
      STOP
      END PROGRAM JEPPSON_CH5
