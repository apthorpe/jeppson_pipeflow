!>
!! Compatibility library implementing interfaces of the Sperry MATH-PACK library
!!
!! # SPERRY MATH-PACK #
!!
!! ## General Description ##
!!
!! The Sperry MATH-PACK system contains approximately 80 FORTRAN V mathematical
!! subprograms.  The system provides the more frequently used techniques in
!! numerical analysis, with each subroutine designed so that the capabilities of
!! the Sperry large-scale equipment are used efficiently (*e.g.*, with respect to
!! storage requirements, computational speed, and accuracy) and the program
!! preparation required prior to calling the subroutine is minimized. The
!! MATH-PACK subroutines are grouped into 14 general categories and are listed in
!! the following section.
!!
!! ## List of Subroutines ##
!!
!! ### Interpolation ###
!!
!! `GNINT` - Gregory-Newton interpolation
!! `GNEXT` - Gregory-Newton extrapolation
!! `GNPOL` - Gregory-Newton polynomial evaluation
!! `BESINT` - Bessel interpolation
!! `STINT` - Stirling interpolation
!! `CDINT` - Gauss central-difference interpolation
!! `AITINT` - Aitken interpolation
!! `YLGINT` - Lagrange interpolation
!! `SPLN1` - spline interpolation
!! `SPLN2` - spline interpolation
!!
!! ### Numerical Integration ###
!!
!! `TRAPNI` - trapezoidal rule
!! `SIM1NI` - Simpson 1/3 rule
!! `SIM3NI` - Simpson 3/8 rule
!! `STEPNI` - variable step integration
!! `GENNI` - generalized numerical quadrature
!! `DOUBNI` - double integration
!! `LGAUSS` - Gauss quadrature abscissas and weights
!! `SIMPTS` - Simpson 1/3 rule abscissas and weights
!!
!! ### Solution of Equations ###
!!
!! `NEWTIT` - Newton-Raphson iteration
!! `WEGIT` - Wegstein iteration
!! `AITIT` - Altken iteration
!! `ROOTCP` - real and complex roots of a real or complex polynomial
!!
!! ### Differentiation ###
!!
!! `DERIV1` - first derivative approximation
!! `DERIV2` - second derivative approximation
!! `NTHDER` - nth derivative of a polynomial
!!
!! ### Polynomial Manipulation ###
!!
!! `GIVZRS` - polynomial coefficients given its zeros
!! `CVALUE` - complex polynomial evaluation
!! `POLYX` - real polynomial multiplication
!! `CPOLYX` - complex polynomial multiplication
!!
!! ### Matrix Manipulation: Real Matrices ###
!!
!! `MXADD` - matrix addition
!! `MXSUB` - matrix subtraction
!! `MXTRN` - matrix transposition
!! `MXMLT` - matrix multiplication
!! `MXSCA` - matrix multiplication by a scalar
!! `MXMDIG` - matrix multiplication by diagonal matrix stored as a vector
!! `GJR` - determinant; inverse; solution of simultaneous equations
!! `MXHOI` - inverse accuracy improvement
!!
!! ### Matrix Manipulation: Complex Matrices ###
!!
!! `CMXADD` - matrix addition
!! `CMXSUB` - matrix subtraction
!! `CMXTRN` - matrix transposition
!! `CMXMLT` - matrix multiplication
!! `CMXSCA` - matrix multiplication by a scalar
!! `CGJR` - determinant; inverse; solution of simultaneous equations
!!
!! ### Matrix Manipulation: Elgenvalues and Eigenvectors ###
!!
!! `TRIDMX` - tridiagonalization of real symmetric matrix
!! `EIGVAL` - elgenvalues of tridiagonal matrix by Sturm sequences
!! `EIGVEC` - elgenvectors of tridiagonal matrix by Wilkinson's method
!!
!! ### Matrix Manipulation: Miscellaneous ###
!!
!! `DGJR` - double-precision determinant; inverse; solution of simultaneous equations
!! `PMXTRI` - polynomial matrix triangularization
!! `SCALE` - polynomial matrix scaling
!! `MXROT` - matrix rotation
!!
!! ### Ordinary Differential Equations ###
!!
!! `EULDE` - Euler's method
!! `HAMDE` - Hamming's method
!! `INVAL` - initial values for differential equation solution
!! `RKDE` - Runge-Kutta method
!! `SOOE` - second-order equations
!! `MRKDE` - reduction of mth order system to system of m first-order equations
!!
!! ### Systems of Equations ###
!!
!! `HJACMX` - Jacobi iteration to determine eigenvalues and eigenvectors of Hermitian matrix
!! `JACMX` - Jacobi iteration to determine eigenvalues and eigenvectors of symmetric matrix
!! `LSIMEQ` - solution to a set of linear simultaneous equations
!! `NSIMEQ` - functional iteration to determine solution to set of nonlinear equations
!!
!! ### Curve Fitting ###
!!
!! `CFSRIE` - coefficients of Fourier series on a continuous range
!! `DFSRIE` - coefficients of Fourier series on a discrete range
!! `FTRANS` - Fourier transform
!! `FITO` - fitted value and derivative values for a least-squares polynomial
!! `ORTHLS` - orthogonal polynomial least-squares curve fitting
!! `FITY` - fitted values for a least-squares polynomial
!! `COEFS` - coefficients of a least-squares polynomial
!!
!! ### Pseudo Random Number Generators ###
!!
!! `NRAND` - interval (0,2**35) generator
!! `MRAND` - modified generator
!! `RANOU` - uniform distribution
!! `RANDN` - normal distribution
!! `RANDEX` - exponential distribution
!!
!! ### Specific Functions ###
!!
!! `BSSL` - zero- and first-order Bessel functions
!! `BESJ` - regular Bessel functions of real argument
!! `BESY` - irregular Bessel functions of real argument
!! `BESI` - regular Bessel functions of imaginary argument
!! `BESK` - irregular Bessel functions of imaginary argument
!! `GAMMA` - gamma function evaluation
!! `LEGEN` - Legendre polynomial evaluation
!! `ARCTNQ` - arctangent of a quotient
!!
!! ## Documentation References ##
!!
!! UNIVAC Systems Reference Library. (1970). UNIVAC large scale systems MATH-PACK program abstracts (UP-4051, rev. 2). Sperry Rand Corporation.
!!
!! UNIVAC Systems Reference Library. (1970). UNIVAC large scale systems MATH-PACK programmers reference (UP-7542, rev. 1). Sperry Rand Corporation.
!!
!! ----
!!
!! ## Editor's Notes ##
!!
!! The library listing above was taken from pages 25-27 of *Mathematical and
!! Statistical Software Index: Second Edition* by Walter G. Albert and Larry K.
!! Whitehead, Manpower and Personnel Division, Brooks Air Force Base, August 1986.
!! <http://www.dtic.mil/dtic/tr/fulltext/u2/a170611.pdf>
!!
!! Some entries have been expanded so that each subroutine is given its own entry.
!!
!! Note that 'FORTRAN V' is Sperry's proprietary extension of FORTRAN 66 (FORTRAN IV).
!!
!! ----
!!
!! Bob Apthorpe <bob.apthorpe@acorvid.com>
!! January 15, 2018 8:49 PM
module alfc_sperry_mathpack
    public :: GNINT
    public :: GNEXT
    public :: GNPOL
    public :: BESINT
    public :: STINT
    public :: CDINT
    public :: AITINT
    public :: YLGINT
    public :: SPLN1
    public :: SPLN2
    public :: TRAPNI
    public :: SIM1NI
    public :: SIM3NI
    public :: STEPNI
    public :: GENNI
    public :: DOUBNI
    public :: LGAUSS
    public :: SIMPTS
    public :: NEWTIT
    public :: WEGIT
    public :: AITIT
!    public :: ROOTCP
    public :: DERIV1
    public :: DERIV2
    public :: NTHDER
    public :: GIVZRS
    public :: CVALUE
    public :: POLYX
    public :: CPOLYX
    public :: MXADD
    public :: MXSUB
    public :: MXTRN
    public :: MXMLT
    public :: MXSCA
    public :: MXMDIG
    public :: GJR
    public :: MXHOI
    public :: CMXADD
    public :: CMXSUB
    public :: CMXTRN
    public :: CMXMLT
    public :: CMXSCA
!    public :: CGJR
    public :: TRIDMX
    public :: EIGVAL
    public :: EIGVEC
!    public :: DGJR
    public :: PMXTRI
    public :: sperry_mathpack_SCALE
    public :: MXROT
    public :: EULDE
    public :: HAMDE
    public :: INVAL
    public :: RKDE
    public :: SOOE
    public :: MRKDE
    public :: HJACMX
    public :: JACMX
    public :: LSIMEQ
    public :: NSIMEQ
    public :: CFSRIE
    public :: DFSRIE
    public :: FTRANS
    public :: FITO
    public :: ORTHLS
    public :: FITY
    public :: COEFS
    public :: NRAND
    public :: MRAND
    public :: RANOU
    public :: RANDN
    public :: RANDEX
    public :: BSSL
    public :: BESJ
    public :: BESY
    public :: BESI
    public :: BESK
    public :: sperry_mathpack_GAMMA
    public :: LEGEN
    public :: ARCTNQ

contains

! ### Interpolation ###

!> Gregory-Newton interpolation
subroutine GNINT()
    implicit none
    continue
    return
end subroutine GNINT

!> Gregory-Newton extrapolation
subroutine GNEXT()
    implicit none
    continue
    return
end subroutine GNEXT

!> Gregory-Newton polynomial evaluation
subroutine GNPOL()
    implicit none
    continue
    return
end subroutine GNPOL

!> Bessel interpolation
subroutine BESINT()
    implicit none
    continue
    return
end subroutine BESINT

!> Stirling interpolation
subroutine STINT()
    implicit none
    continue
    return
end subroutine STINT

!> Gauss central-difference interpolation
subroutine CDINT()
    implicit none
    continue
    return
end subroutine CDINT

!> Aitken interpolation
subroutine AITINT()
    implicit none
    continue
    return
end subroutine AITINT

!> Lagrange interpolation
subroutine YLGINT()
    implicit none
    continue
    return
end subroutine YLGINT

!> Spline interpolation
subroutine SPLN1()
    implicit none
    continue
    return
end subroutine SPLN1

!> Spline interpolation
subroutine SPLN2()
    implicit none
    continue
    return
end subroutine SPLN2

! ### Numerical Integration ###

!> Trapezoidal rule
subroutine TRAPNI()
    implicit none
    continue
    return
end subroutine TRAPNI

!> Simpson 1/3 rule
subroutine SIM1NI()
    implicit none
    continue
    return
end subroutine SIM1NI

!> Simpson 3/8 rule
subroutine SIM3NI()
    implicit none
    continue
    return
end subroutine SIM3NI

!> Variable step integration
subroutine STEPNI()
    implicit none
    continue
    return
end subroutine STEPNI

!> Generalized numerical quadrature
subroutine GENNI()
    implicit none
    continue
    return
end subroutine GENNI

!> Double integration
subroutine DOUBNI()
    implicit none
    continue
    return
end subroutine DOUBNI

!> Gauss quadrature abscissas and weights
subroutine LGAUSS()
    implicit none
    continue
    return
end subroutine LGAUSS

!> Simpson 1/3 rule abscissas and weights
subroutine SIMPTS()
    implicit none
    continue
    return
end subroutine SIMPTS

! ### Solution of Equations ###

!> Newton-Raphson iteration
subroutine NEWTIT()
    implicit none
    continue
    return
end subroutine NEWTIT

!> Wegstein iteration
subroutine WEGIT()
    implicit none
    continue
    return
end subroutine WEGIT

!> Altken iteration
subroutine AITIT()
    implicit none
    continue
    return
end subroutine AITIT

!  !> Real and complex roots of a real or complex polynomial
!  !! Documentation of this routine may be found on pages 129-135 of 
!  !! "Direct Contact Heat Transfer between Two Immiscible Liquids in
!  !! Laminar Flow Between Parallel Plates", Richard W. Johnson, Harold R.
!  !! Jacobs, and Robert F. Boehm, IDO-1549-1, December 1975
!  !! https://www.osti.gov/scitech/biblio/7255733
!  subroutine ROOTCP(A, N, EPS, KMAX, X, J, K)
!      implicit none
!  
!  !> An array of N+1 elements containing the coefficients of the
!  !! polynomlal. 
!      complex, dimension(N+1), intent(in) :: A
!  
!  !> The degree of the polynomial. N is also the number of elements in the
!  !! X array. 
!      integer, intent(in) :: N
!  
!  !> The maximum permissible relatlve error between successive
!  !! approximations to a root. 
!      real, intent(in) :: EPS
!  
!  !> The maximum number of iterations to be performed in findlng the roots
!  !! of the polynomial.
!      integer, intent(in) :: KMAX
!  
!  !> An array of N elements containing the computed roots of the polynomial.
!      complex, dimension(N), intent(out) :: X
!  
!  !> Root convergence indicator.
!  !!
!  !! If J == N, all the roots converged successfully.
!  !!
!  !! If J < N, then the jth root fails to converge uslng the maximum nunber
!  !! of iteretions. 
!  
!      integer, intent(out) :: J
!  
!  !> A statement number in the calling program. It must be preceded by $ in
!  !! the calling sequence. DEPRECATED: Alternate return points are not
!  !! supported in this library; omit this argument and add error handling
!  !! code immediately after the call to ROOTCP.
!      integer, intent(in), optional :: K
!  
!      continue
!  
!      ! Dummy code
!      X = 0.0
!      J = 1
!  
!      return
!  end subroutine ROOTCP

! ### Differentiation ###

!> First derivative approximation
subroutine DERIV1()
    implicit none
    continue
    return
end subroutine DERIV1

!> Second derivative approximation
subroutine DERIV2()
    implicit none
    continue
    return
end subroutine DERIV2

!> Nth derivative of a polynomial
subroutine NTHDER()
    implicit none
    continue
    return
end subroutine NTHDER

! ### Polynomial Manipulation ###

!> Polynomial coefficients given its zeros
subroutine GIVZRS()
    implicit none
    continue
    return
end subroutine GIVZRS

!> Complex polynomial evaluation
subroutine CVALUE()
    implicit none
    continue
    return
end subroutine CVALUE

!> Real polynomial multiplication
subroutine POLYX()
    implicit none
    continue
    return
end subroutine POLYX

!> Complex polynomial multiplication
subroutine CPOLYX()
    implicit none
    continue
    return
end subroutine CPOLYX

! ### Matrix Manipulation: Real Matrices ###

!> Matrix addition
subroutine MXADD()
    implicit none
    continue
    return
end subroutine MXADD

!> Matrix subtraction
subroutine MXSUB()
    implicit none
    continue
    return
end subroutine MXSUB

!> Matrix transposition
subroutine MXTRN()
    implicit none
    continue
    return
end subroutine MXTRN

!> Matrix multiplication
subroutine MXMLT()
    implicit none
    continue
    return
end subroutine MXMLT

!> Matrix multiplication by a scalar
subroutine MXSCA()
    implicit none
    continue
    return
end subroutine MXSCA

!> Matrix multiplication by diagonal matrix stored as a vector
subroutine MXMDIG()
    implicit none
    continue
    return
end subroutine MXMDIG

!> Determinant; inverse; solution of simultaneous equations
!!
!! GJR is a subroutine which solves simultaneous equations, computes a
!! determinant, inverts a matrix, or does any combination of these three
!! operations, by using a Gauss-Jordan elimination technique with column
!! pivoting.
!! 
!! The procedure for using GJR is as follows:
!! 
!! Calling statement: CALL GJR(A, NC, NR, N, MC, $K, JC, V)
!! 
!! Note that in this compatibility library, $K is ignored; external error
!! handling should be added to the original code to mimic the alternate
!! return point behavior allowed in Sperry's FORTRAN V.
!!
!! Notes on usage of row dimension arguments N and NR:
!!
!! The arguments N and NR refer to the row dimensions of the A matrix. N
!! gives the number of rows operated on by the subroutine, while NR refers
!! to the total number of rows in the matrix as dimensioned by the calling
!! program. NR is used only in the dimension statement of the subroutine.
!! Through proper use of these parameters, the user may specify that only a
!! submatrix, instead of the entire matrix, be operated on by the
!! subroutine.
!!
!! This compatibility routine depends directly on LAPACK routines ILAENV,
!! SGETRF, SGETRI, and SGESV. The internal routine set_gjr_status sets JC(1)
!! and V if an error occurs consistent with the original GJR routine.
subroutine GJR(A, NC, NR, N, MC, K, JC, V)
!    use diag, only: show_mat
    implicit none

! From LAPACK
    integer :: ILAENV
    external :: ILAENV

!> NC is an integer representing the maximum number of columns of the array A.

    integer, intent(in) :: NC

!> NR is an integer representing the maximum number of rows of the array A.

    integer, intent(in) :: NR

!> A is the matrix whose inverse or determinant is to be determined. If
!! simultaneous equations are solved, the last MC-N columns of the matrix
!! are the constant vectors of the equations to be solved. On output, if
!! the inverse is computed, it is stored in the first N columns of A. If
!! simultaneous equations are solved, the last MC-N columns contain the
!! solution vectors. A is a real array.

    real, dimension(NR, NC), intent(in out) :: A

!> N is an integer representing the number of rows of the array A to be
!! operated on.

    integer, intent(in) :: N

!> MC is the number of columns of the array A, representing the
!! coefficient matrix if simultaneous equations are being solved; otherwise
!! it is a dummy variable.

    integer, intent(in) :: MC

!> K is a statement number in the calling program to which control is
!! returned if an overflow or singularity is detected. 1) If an overflow is
!! detected, JC(1) is set to the negative of the last correctly completed
!! row of the reduction and control is then returned to statement number K
!! in the calling program. 2) If a singularity is detected, JC(1) is set to
!! the number of the last correctly completed row, and V is set to (0.,0.)
!! if the determinant was to be computed. Control is then returned to
!! statement number K in the calling program.
!!
!! DEPRECATED: Alternate return points are not supported in this library;
!! this argument is ignored. Add error handling code immediately after the
!! call to GJR which checks the return value of JC(1) and V.

    integer, intent(in) :: K

!> JC is a one dimensional permutation array of N elements which is used
!! for permuting the rows and columns of A if an inverse is being computed.
!! If an inverse is not computed, this array must have at least one cell
!! for the error return identification. On output, JC(1) is N if control is
!! returned normally.

    integer, dimension(N), intent(in out) :: JC

!> V is a real variable. On input V is the option indicator, set
!! as follows:
!!     1. invert matrix
!!     2. compute determinant
!!     3. do 1. and 2.
!!     4. solve system of equations 
!!     5. do 1. and 4.
!!     6. do 2. and 4.
!!     7. do 1., 2. and 4.

    real, dimension(2), intent(in out) :: V

! Local variables

    real, dimension(:, :), allocatable :: AA
    real, dimension(:, :), allocatable :: BB
    integer :: INFO
    integer :: NRHS
    integer :: LDA
    integer :: LDB

    real :: DET

    integer :: NB
    integer :: LWORK
    real, dimension(:), allocatable :: WORK

    logical :: L_INVERT
    logical :: L_DET
    logical :: L_SOLVE

    integer :: I
!    integer :: J

!1   format(A)
!5   format(A, 'matrix A (', I3, ' columns by ', I3, ' rows)')
!10  format(15X,:,*(X, I15))
!20  format(I15,:,*(X, ES15.8))
!30  format(A, ' is ', I3)
!40  format(A, ' is ', G15.8)
!50  format(A, ' is ', L1)

    continue

!    write(6, 1) 'Entering GJR:'
!    write(6, 30) 'NC', NC
!    write(6, 30) 'NR', NR
!    write(6, 30) 'N', N
!    write(6, 30) 'MC', MC
!    write(6, 40) 'V(1)', V(1)
!    write(6, 40) 'V(2)', V(2)

!    call show_mat(A, 'Full ')

!    call show_mat(A(1:MC,1:N), 'Sub')

!    write(6, 30) 'int(V(1))', int(V(1))

    ! Detect operating mode from V(1)
    L_INVERT = .false.
    L_DET = .false.
    L_SOLVE = .false.
    select case (int(V(1)))
    case (1)
        L_INVERT = .true.
    case (2)
        L_DET = .true.
    case (3)
        L_INVERT = .true.
        L_DET = .true.
    case (4)
        L_SOLVE = .true.
    case (5)
        L_INVERT = .true.
        L_SOLVE = .true.
    case (6)
        L_DET = .true.
        L_SOLVE = .true.
    case (7)
        L_INVERT = .true.
        L_DET = .true.
        L_SOLVE = .true.
    case default
        ! Unknown operating mode
        JC(1) = 0
        return
    end select

    ! Clear LAPACK status code
    INFO = 0

!    write(6, 50) 'L_INVERT', L_INVERT
!    write(6, 50) 'L_DET', L_DET
!    write(6, 50) 'L_SOLVE', L_SOLVE

    ! Calculate inverse and/or determinant
    if (L_INVERT .or. L_DET) then
        ! Copy original NxN matrix A to LAPACK matrix AA
        if (allocated(AA)) deallocate(AA)
        allocate(AA(N, N))
        AA(1:N, 1:N) = A(1:N, 1:N)

        JC = 0

        LDA = N
        INFO = 0

        ! Factorize A into LU Matrices
        call SGETRF(N, N, AA, LDA, JC, INFO)

        ! Check INFO
        call set_gjr_status(INFO, JC, V)

        ! Find determinant unless error has occurred during LU factorization
        if (L_DET .and. (INFO == 0)) then
            DET = 1.0
            do I = 1, N
                DET = DET * AA(I, I)
                if (JC(I) /= I) then
                    DET = -DET
                end if
            end do
            ! TODO: Is this where the determinant goes?
            V(2) = DET
        end if

        ! Invert matrix unless error has occurred during LU factorization
        if (L_INVERT .and. (INFO == 0)) then
            ! Get optimal dimension for scratch array
            ! NB: Guessing at OPTS and N1-N4 arguments
            NB = ILAENV(1, 'SGETRI', '', -1, -1, -1, -1)
            if (NB <= 1) then
                NB = max(1, N)
            end if
            LWORK = N * NB

            ! Create scratch array for SETGRI
            if (allocated(WORK)) deallocate(WORK)
            allocate(WORK(LWORK))
            WORK = 0.0

            ! Calculate inverse of LU matrix
            call SGETRI(N, AA, LDA, JC, WORK, LWORK, INFO)

            ! Check INFO
            call set_gjr_status(INFO, JC, V)

            ! Copy LAPACK matrix AA back over original NxN matrix in A 
            A(1:N, 1:N) = AA(1:N, 1:N)

        end if
    end if

    ! Solve matrix
    if (L_SOLVE .and. (INFO == 0)) then
        NRHS = MC - N
        LDA = N
        LDB = N
!        INFO = 0

        ! Copy original NxN matrix A to LAPACK matrix AA
        if (allocated(AA)) deallocate(AA)
        allocate(AA(N, N))
        AA(1:N, 1:N) = A(1:N, 1:N)

        ! Transpose NRHS columns of N rows starting at column N+1 into a 
        ! row vector NRHS rows by N cols (BB)
        if (allocated(BB)) deallocate(BB)
        allocate(BB(MC-N, N))
        BB(1:NRHS, 1:N) = reshape(source=A(1:N, (N+1):MC), shape=[NRHS, N])
!        BB(1:MC-N, 1:N) = reshape(source=A(1:N, MC), shape=[1, N])

!        write(6, 30) 'MC-N', MC-N
!        write(6, 30) 'N', N
!        write(6, 30) 'MC', MC
!        write(6, 30) 'NRHS', NRHS
!        write(6, 30) 'LDA', LDA
!        write(6, 30) 'LDB', LDB
!        write(6, 30) 'INFO', INFO

!        write(6, 1) ''
!        write(6, 5) 'AA ', N, N
!        write(6, 1) ''

!        write(6, 10) (I, I=1,N)
!        do J = 1, N
!            write(6, 20) J, (AA(I,J), I=1,N)
!        end do

!        write(6, 1) ''
!        write(6, 5) 'BB ', MC-N, N
!        write(6, 1) ''

!        write(6, 10) (I, I=1,MC-N)
!        do J = 1, N
!            write(6, 20) J, (BB(I,J), I=1,MC-N)
!        end do

        ! Solve general matrix (LAPACK)
        call SGESV(N, NRHS, AA, LDA, JC, BB, LDB, INFO)

        ! Copy LAPACK matrix AA back over original NxN matrix in A 
        A(1:N, 1:N) = AA(1:N, 1:N)
        ! Copy LAPACK solution vector(s) BB to original position in 
        ! A matrix (transpose into columns N+1 to MC
        A(1:N, (N+1):MC) = reshape(source=BB(1:NRHS, 1:N), shape=[N, NRHS])
!        A(1:N, (N+1):MC) = reshape(source=BB(1:(MC-N), 1:N), shape=[N, 1])

!!        write(6, "('INFO = ', I3)") INFO
!        if (INFO == 0) then
!!            write(6, "('OK')")
!            JC(1) = N
!        else if (INFO > 0) then
!!            write(6, "('SIGNULAR')")
!            JC(1) = INFO - 1
!            V = 0.0
!        else
!!            write(6, "('FAILURE')")
!            JC(1) = INFO + 1
!        end if
        call set_gjr_status(INFO, JC, V)

    end if

!    write(6, 1) 'Leaving GJR:'

    return

contains

    !> Set GJR status code from LAPACK status code
    subroutine set_gjr_status(INFO, JC, V)
        !> Status code
        integer, intent(in) :: INFO

        !> Pivot/status array
        integer, dimension(:), intent(in out) :: JC

        !> Control and response vector
        real, dimension(:), intent(in out) :: V

        continue

!        write(6, "('INFO = ', I3)") INFO
        if (INFO == 0) then
!            write(6, "('OK')")
            JC(1) = N
        else if (INFO > 0) then
!            write(6, "('SIGNULAR')")
            JC(1) = INFO - 1
            V = 0.0
        else
!            write(6, "('FAILURE')")
            JC(1) = INFO + 1
        end if

        return
    end subroutine set_gjr_status
end subroutine GJR

!> Inverse accuracy improvement
subroutine MXHOI()
    implicit none
    continue
    return
end subroutine MXHOI

! ### Matrix Manipulation: Complex Matrices ###

!> Matrix addition
subroutine CMXADD()
    implicit none
    continue
    return
end subroutine CMXADD

!> Matrix subtraction
subroutine CMXSUB()
    implicit none
    continue
    return
end subroutine CMXSUB

!> Matrix transposition
subroutine CMXTRN()
    implicit none
    continue
    return
end subroutine CMXTRN

!> Matrix multiplication
subroutine CMXMLT()
    implicit none
    continue
    return
end subroutine CMXMLT

!> Matrix multiplication by a scalar
subroutine CMXSCA()
    implicit none
    continue
    return
end subroutine CMXSCA

!  !> Complex determinant; inverse; solution of simultaneous equations
!  !!
!  !! CGJR is a subroutine which solves simultaneous equations, computes a
!  !! determinant, inverts a matrix, or does any combination of these three
!  !! operations, by using a Gauss-Jordan elimination technique with column
!  !! pivoting.
!  !! 
!  !! The procedure for using CGJR is as follows:
!  !! 
!  !! Calling statement: CALL CGJR(A, NC, NR, N, MC, $K, JC, V)
!  !! 
!  !! Note that in this compatibility library, $K is ignored; external error
!  !! handling should be added to the original code to mimic the alternate
!  !! return point behavior allowed in Sperry's FORTRAN V.
!  !!
!  !! Notes on usage of row dimension arguments N and NR:
!  !!
!  !! The arguments N and NR refer to the row dimensions of the A matrix. N
!  !! gives the number of rows operated on by the subroutine, while NR refers
!  !! to the total number of rows in the matrix as dimensioned by the calling
!  !! program. NR is used only in the dimension statement of the subroutine.
!  !! Through proper use of these parameters, the user may specify that only a
!  !! submatrix, instead of the entire matrix, be operated on by the
!  !! subroutine.
!  subroutine CGJR(A, NC, NR, N, MC, K, JC, V)
!      implicit none
!  
!  !> NC is an integer representing the maximum number of columns of the array A.
!  
!      integer, intent(in) :: NC
!  
!  !> NR is an integer representing the maximum number of rows of the array A.
!  
!      integer, intent(in) :: NR
!  
!  !> A is the matrix whose inverse or determinant is to be determined. If
!  !! simultaneous equations are solved, the last MC-N columns of the matrix
!  !! are the constant vectors of the equations to be solved. On output, if
!  !! the inverse is computed, it is stored in the first N columns of A. If
!  !! simultaneous equations are solved, the last MC-N columns contain the
!  !! solution vectors. A is a complex array.
!  
!      complex, dimension(NC, NR), intent(in out) :: A
!  
!  !> N is an integer representing the number of rows of the array A to be
!  !! operated on.
!  
!      integer, intent(in) :: N
!  
!  !> MC is the number of columns of the array A, representing the
!  !! coefficient matrix if simultaneous equations are being solved; otherwise
!  !! it is a dummy variable.
!  
!      integer, intent(in) :: MC
!  
!  !> K is a statement number in the calling program to which control is
!  !! returned if an overflow or singularity is detected. 1) If an overflow is
!  !! detected, JC(1) is set to the negative of the last correctly completed
!  !! row of the reduction and control is then returned to statement number K
!  !! in the calling program. 2) If a singularity is detected, JC(1) is set to
!  !! the number of the last correctly completed row, and V is set to (0.,0.)
!  !! if the determinant was to be computed. Control is then returned to
!  !! statement number K in the calling program.
!  !!
!  !! DEPRECATED: Alternate return points are not supported in this library;
!  !! this argument is ignored. Add error handling code immediately after the
!  !! call to CGJR which checks the return value of JC(1) and V.
!  
!      integer, intent(in) :: K
!  
!  !> JC is a one dimensional permutation array of N elements which is used
!  !! for permuting the rows and columns of A if an inverse is being computed.
!  !! If an inverse is not computed, this array must have at least one cell
!  !! for the error return identification. On output, JC(1) is N if control is
!  !! returned normally.
!  
!      integer, dimension(N), intent(in out) :: JC
!  
!  !> V is a complex variable. On input REAL(V) is the option indicator, set
!  !! as follows:
!  !!     1. invert matrix
!  !!     2. compute determinant
!  !!     3. do 1. and 2.
!  !!     4. solve system of equations 
!  !!     5. do 1. and 4.
!  !!     6. do 2. and 4.
!  !!     7. do 1., 2. and 4.
!  
!      complex, intent(in out) :: V
!  
!      continue
!  
!      JC = 0
!      JC(1) = 1
!  
!      V = 0.0
!  
!      return
!  end subroutine CGJR

! ### Matrix Manipulation: Elgenvalues and Eigenvectors ###

!> Tridiagonalization of real symmetric matrix
subroutine TRIDMX()
    implicit none
    continue
    return
end subroutine TRIDMX

!> Elgenvalues of tridiagonal matrix by Sturm sequences
subroutine EIGVAL()
    implicit none
    continue
    return
end subroutine EIGVAL

!> Elgenvectors of tridiagonal matrix by Wilkinson's method
subroutine EIGVEC()
    implicit none
    continue
    return
end subroutine EIGVEC

! ### Matrix Manipulation: Miscellaneous ###

!  !> Double-precision determinant; inverse; solution of simultaneous equations
!  !!
!  !! DGJR is a subroutine which solves simultaneous equations, computes a
!  !! determinant, inverts a matrix, or does any combination of these three
!  !! operations, by using a Gauss-Jordan elimination technique with column
!  !! pivoting.
!  !! 
!  !! The procedure for using DGJR is as follows:
!  !! 
!  !! Calling statement: CALL DGJR(A, NC, NR, N, MC, $K, JC, V)
!  !! 
!  !! Note that in this compatibility library, $K is ignored; external error
!  !! handling should be added to the original code to mimic the alternate
!  !! return point behavior allowed in Sperry's FORTRAN V.
!  !!
!  !! Notes on usage of row dimension arguments N and NR:
!  !!
!  !! The arguments N and NR refer to the row dimensions of the A matrix. N
!  !! gives the number of rows operated on by the subroutine, while NR refers
!  !! to the total number of rows in the matrix as dimensioned by the calling
!  !! program. NR is used only in the dimension statement of the subroutine.
!  !! Through proper use of these parameters, the user may specify that only a
!  !! submatrix, instead of the entire matrix, be operated on by the
!  !! subroutine.
!  subroutine DGJR(A, NC, NR, N, MC, K, JC, V)
!      implicit none
!  
!  !> NC is an integer representing the maximum number of columns of the array A.
!  
!      integer, intent(in) :: NC
!  
!  !> NR is an integer representing the maximum number of rows of the array A.
!  
!      integer, intent(in) :: NR
!  
!  !> A is the matrix whose inverse or determinant is to be determined. If
!  !! simultaneous equations are solved, the last MC-N columns of the matrix
!  !! are the constant vectors of the equations to be solved. On output, if
!  !! the inverse is computed, it is stored in the first N columns of A. If
!  !! simultaneous equations are solved, the last MC-N columns contain the
!  !! solution vectors. A is a double precision array.
!  
!      double precision, dimension(NC, NR), intent(in out) :: A
!  
!  !> N is an integer representing the number of rows of the array A to be
!  !! operated on.
!  
!      integer, intent(in) :: N
!  
!  !> MC is the number of columns of the array A, representing the
!  !! coefficient matrix if simultaneous equations are being solved; otherwise
!  !! it is a dummy variable.
!  
!      integer, intent(in) :: MC
!  
!  !> K is a statement number in the calling program to which control is
!  !! returned if an overflow or singularity is detected. 1) If an overflow is
!  !! detected, JC(1) is set to the negative of the last correctly completed
!  !! row of the reduction and control is then returned to statement number K
!  !! in the calling program. 2) If a singularity is detected, JC(1) is set to
!  !! the number of the last correctly completed row, and V is set to (0.,0.)
!  !! if the determinant was to be computed. Control is then returned to
!  !! statement number K in the calling program.
!  !!
!  !! DEPRECATED: Alternate return points are not supported in this library;
!  !! this argument is ignored. Add error handling code immediately after the
!  !! call to CGJR which checks the return value of JC(1) and V.
!  
!      integer, intent(in) :: K
!  
!  !> JC is a one dimensional permutation array of N elements which is used
!  !! for permuting the rows and columns of A if an inverse is being computed.
!  !! If an inverse is not computed, this array must have at least one cell
!  !! for the error return identification. On output, JC(1) is N if control is
!  !! returned normally.
!  
!      integer, dimension(N), intent(in out) :: JC
!  
!  !> V is a double precision variable. On input REAL(V) is the option
!  !! indicator, set as follows:
!  !!     1. invert matrix
!  !!     2. compute determinant
!  !!     3. do 1. and 2.
!  !!     4. solve system of equations 
!  !!     5. do 1. and 4.
!  !!     6. do 2. and 4.
!  !!     7. do 1., 2. and 4.
!  
!      double precision, intent(in out) :: V
!  
!      continue
!  
!      JC = 0
!      JC(1) = 1
!  
!      V = 0.0
!  
!      return
!  end subroutine DGJR

!> Polynomial matrix triangularization
subroutine PMXTRI()
    implicit none
    continue
    return
end subroutine PMXTRI

!> Polynomial matrix scaling
subroutine sperry_mathpack_SCALE()
    implicit none
    continue
    return
end subroutine sperry_mathpack_SCALE

!> Matrix rotation
subroutine MXROT()
    implicit none
    continue
    return
end subroutine MXROT

! ### Ordinary Differential Equations ###

!> Euler's method
subroutine EULDE()
    implicit none
    continue
    return
end subroutine EULDE

!> Hamming's method
subroutine HAMDE()
    implicit none
    continue
    return
end subroutine HAMDE

!> Initial values for differential equation solution
subroutine INVAL()
    implicit none
    continue
    return
end subroutine INVAL

!> Runge-Kutta method
subroutine RKDE()
    implicit none
    continue
    return
end subroutine RKDE

!> Second-order equations
subroutine SOOE()
    implicit none
    continue
    return
end subroutine SOOE

!> Reduction of mth order system to system of m first-order equations
subroutine MRKDE()
    implicit none
    continue
    return
end subroutine MRKDE

! ### Systems of Equations ###

!> Jacobi iteration to determine eigenvalues and eigenvectors of Hermitian matrix
subroutine HJACMX()
    implicit none
    continue
    return
end subroutine HJACMX

!> Jacobi iteration to determine eigenvalues and eigenvectors of symmetric matrix
subroutine JACMX()
    implicit none
    continue
    return
end subroutine JACMX

!> Solution to a set of linear simultaneous equations
subroutine LSIMEQ()
    implicit none
    continue
    return
end subroutine LSIMEQ

!> Functional iteration to determine solution to set of nonlinear equations
subroutine NSIMEQ()
    implicit none
    continue
    return
end subroutine NSIMEQ

! ### Curve Fitting ###

!> Coefficients of Fourier series on a continuous range
subroutine CFSRIE()
    implicit none
    continue
    return
end subroutine CFSRIE

!> Coefficients of Fourier series on a discrete range
subroutine DFSRIE()
    implicit none
    continue
    return
end subroutine DFSRIE

!> Fourier transform
subroutine FTRANS()
    implicit none
    continue
    return
end subroutine FTRANS

!> Fitted value and derivative values for a least-squares polynomial
subroutine FITO()
    implicit none
    continue
    return
end subroutine FITO

!> Orthogonal polynomial least-squares curve fitting
subroutine ORTHLS()
    implicit none
    continue
    return
end subroutine ORTHLS

!> Fitted values for a least-squares polynomial
subroutine FITY()
    implicit none
    continue
    return
end subroutine FITY

!> Coefficients of a least-squares polynomial
subroutine COEFS()
    implicit none
    continue
    return
end subroutine COEFS

! ### Pseudo Random Number Generators ###

!> Interval (0,2**35) generator
subroutine NRAND()
    implicit none
    continue
    return
end subroutine NRAND

!> Modified generator
subroutine MRAND()
    implicit none
    continue
    return
end subroutine MRAND

!> Uniform distribution
subroutine RANOU()
    implicit none
    continue
    return
end subroutine RANOU

!> Normal distribution
subroutine RANDN()
    implicit none
    continue
    return
end subroutine RANDN

!> Exponential distribution
subroutine RANDEX()
    implicit none
    continue
    return
end subroutine RANDEX

! ### Specific Functions ###

!> Zero- and first-order Bessel functions
subroutine BSSL()
    implicit none
    continue
    return
end subroutine BSSL

!> Regular Bessel functions of real argument
subroutine BESJ()
    implicit none
    continue
    return
end subroutine BESJ

!> Irregular Bessel functions of real argument
subroutine BESY()
    implicit none
    continue
    return
end subroutine BESY

!> Regular Bessel functions of imaginary argument
subroutine BESI()
    implicit none
    continue
    return
end subroutine BESI

!> Irregular Bessel functions of imaginary argument
subroutine BESK()
    implicit none
    continue
    return
end subroutine BESK

!> Gamma function evaluation
subroutine sperry_mathpack_GAMMA()
    implicit none
    continue
    return
end subroutine sperry_mathpack_GAMMA

!> Legendre polynomial evaluation
subroutine LEGEN()
    implicit none
    continue
    return
end subroutine LEGEN

!> Arctangent of a quotient
subroutine ARCTNQ()
    implicit none
    continue
    return
end subroutine ARCTNQ

end module alfc_sperry_mathpack
