#!/bin/sh

# Set environment to use ifort
. /opt/intel/bin/compilervars.sh intel64
. /opt/intel/mkl/bin/mklvars.sh  intel64 mod

# Set ifort as Fortran compiler
FC=ifort

# Set compilation options
# Debug
CFLAGS="-warn all -O0 -g -check bounds -pg -zero -save -traceback -fpe0"
# Release
# CFLAGS="-warn all -O3 -g -zero -save -traceback -fpe0"

# Set link options
LDFLAGS="-L/usr/lib/lapack -llapack"

# Compile
$FC -c $CFLAGS ./alfc_sperry_mathpack.f90 
$FC -c $CFLAGS ./JEPPSON_CH5.f90 

# Link
$FC -o ./JEPPSON_CH5_if ./alfc_sperry_mathpack.o ./JEPPSON_CH5.o $LDFLAGS
