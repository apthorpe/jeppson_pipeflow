#!/bin/sh

# Set path to local build of gcc
GCCBASE=/home/apthorpe/bin/gcc/5.2.0
# LD_LIBRARY_PATH=$GCCBASE/lib64:$GCCBASE/lib:$LD_LIBRARY_PATH:
LD_LIBRARY_PATH=$GCCBASE/lib64:$GCCBASE/lib

# Set gfortran as Fortran compiler
FC=$GCCBASE/bin/gfortran

# Set compilation options
# Debug
CFLAGS="-Wall -pedantic -Og -g -fbounds-check -pg -finit-local-zero -fno-automatic -fbacktrace -ffpe-trap=invalid,zero,overflow"
# Release
# CFLAGS="-Wall -O3 -g -finit-local-zero -fno-automatic -fbacktrace -ffpe-trap=invalid,zero,overflow"

# Set link options
LDFLAGS="-L/usr/lib/lapack -llapack"

# Compile
$FC -c $CFLAGS ./alfc_sperry_mathpack.f90 
$FC -c $CFLAGS ./JEPPSON_CH6B.f90 

# Link
$FC -o ./JEPPSON_CH6B_gf ./alfc_sperry_mathpack.o ./JEPPSON_CH6B.o $LDFLAGS
