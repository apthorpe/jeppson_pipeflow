!> Corrective loop Newton-Raphson pipe flow solver
      PROGRAM JEPPSON_CH6B
!
      use alfc_sperry_mathpack, only: GJR
!
      REAL D(50),L(50),K(50),CHW(50),QI(50),DQ(50),DR(50,51),V(2),A(3), &
      B(3),HO(3),DELEV(3)
      INTEGER LP(42,7),NN(42),LO(3),LLP(3),LOP(50,4),NLOP(50)
! Integer array needed for pivot array in GJR (see below) 
      INTEGER, dimension(50) :: JC
! NP-NO. OF PIPES, NL-NO. OF LOOPS, MAXITER-MAX. NO.
! OF ITERATIONS ALLOWED, NPUMP-NO. OF PUMPS,
! NSL-NO. OF PSEUDO LOOPS.
98    READ(5,100,END=99) NP,NL,MAXITER,NPUMP,NSL
!      write(6,"('CARD ', I3, ': NP = ', I3, ', NL = ', I3, "            &
!            // "', MAXITER = ', I3, ', NPUMP = ', I3, ', NSL = ', I3)") &
!            1,NP,NL,MAXITER,NPUMP,NSL
      NLP=NL+1
100   FORMAT(10I5)
! II-PIPE NO., D(II)-DIAMETER OF PIPE IN INCHES, L(II)-
! LENGTH OF PIPE IN FT, CHW(II)-HAZEN WILLIAMS COEF.,
! QI(II)-INITIAL FLOW SATISFYING CONTINUITY EQS.
      DO 1 I=1,NP
      READ(5,101) II,D(II),L(II),CHW(II),QI(II)
!      write(6,"('CARD ', I3, ': II = ', I3, ', D = ', ES12.4, "         &
!            // "', L = ', ES12.4, ', CHW = ', ES12.4, ', QI = ', "      &
!            // "ES12.4)") I+1,II,D(II),L(II),CHW(II),QI(II)
      D(II)=D(II)/12.
      K(II)=4.77*L(II)/(D(II)**4.87 * CHW(II)**1.852)
1     CONTINUE
101   FORMAT(I5,7F10.5)
      DO 2 I=1,NL
      DQ(I)=0.
! NNP IS THE NUMBER OF PIPES AROUND THE LOOP
! LP(I,J) ARE THE PIPE NO. AROUND THE LOOP. IF
! COUNTERCLOCKWISE THIS NO. IS -
      READ(5,100) NNP,(LP(I,J),J=1,NNP)
!      write(6,"('CARD ', I3, ': LP(', I3, ',1:', I3, ') = ', "          &
!            // "*(I3, :, ', '))") I+NP+1, I, NNP, (LP(I,J),J=1,NNP) 
      NN(I)=NNP
2     CONTINUE
! LLP(I)-LINE NO. CONTAINING PUMP (MINUS IF
! COUNTERCLOCKWISE, A, B, HO-PUMP CHAR
      IF (NPUMP .EQ. 0) GO TO 30
      DO 31 I=1,NPUMP
      READ(5,101) LLP(I),A(I),B(I),HO(I)
!      write(6,"('CARD ', I3, ': I = ', I3, ', LLP = ', I3, "            &
!            // "', A = ', ES12.4, ', B = ', ES12.4, ', HO = ', "        &
!            // "ES12.4)") I+NP+NL+1,I,LLP(I),A(I),B(I),HO(I)
31    CONTINUE
! LO(I)-NO. OF PSEUDO LOOP, DELEV(I)-ELEV. DIFF. ON
! RIGHT OF IN ENERGY EQ.
      DO 32 I=1,NSL
      READ(5,101) LO(I),DELEV(I)
!      write(6,"('CARD ', I3, ': I = ', I3, ', LO = ', I3, ', "          &
!           // "DELEV = ', ES12.4)") I+NP+NL+NPUMP+1, I, LO(I), DELEV(I)
32    CONTINUE
30    CONTINUE
      DO 50 I=1,NP
      NLO=0
      DO 151 L1=1,NL
      NNP=NN(L1)
      DO 251 KK=1,NNP
      IF (ABS(LP(L1 ,KK)) .NE.I) GO TO 251
      NLO=NLO+1
      LOP(I,NLO)=L1 *LP(L1 ,KK) / I
251   CONTINUE
151   CONTINUE
      NLOP(I)=NLO
50    CONTINUE
      NCT=0
10    CONTINUE
      SSUM=0.
      DO 103 I=1,NL
      DO 12 J=1,NLP
      DR(I,J)=0.
12    CONTINUE
      NNP=NN(I)
      DO 203 J=1,NNP
      IJ=LP(I,J)
      IIJ=ABS(IJ)
      Q=QI(IIJ)
      NLO=NLOP(IIJ)
      DO 52 KK=1,NLO
      L1 = LOP(IIJ ,KK)
      LL=ABS(L1)
      Q=Q+FLOAT(L1/LL)*DQ(LL)
52    CONTINUE
      QE=ABS(Q)**.852
      FAC=IJ/IIJ
      DR(I,NLP)=DR(I,NLP)+FAC*K(IIJ)*Q*QE
      DO 53 KK=1,NLO
      L1 = LOP(IIJ ,KK)
      LL=ABS(L1)
      DR(I,LL)=DR(I,LL)+FAC*FLOAT(L1/LL)*1.852*K(IIJ)*QE
53    CONTINUE
203   CONTINUE
103   CONTINUE
      IF(NSL .EQ. 0) GO TO 40
      DO 133 I=1,NSL
      II=LO(I)
!      write(6, "('I = ', I3, ', II=LO(I) = ', I3)") I, II
      DR(II,NLP)=DR(II,NLP)-DELEV(I)
      NNP=NN(II)
      DO 233 IK=1,NPUMP
      IL=ABS(LLP(IK))
      DO 333 KK=1,NNP
      IF(IL .NE. ABS(LP(II,KK))) GO TO 333
! Original equation in text is missing a closing paren
!      Q=ABS(FLOAT(LLP(IK)/IL)*QI(IL)+DQ(II)
! Possible interpretations are:
! 1)  Q=ABS(FLOAT(LLP(IK)/IL))*QI(IL)+DQ(II)
! 2)  Q=ABS(FLOAT(LLP(IK)/IL)*QI(IL))+DQ(II)
! 3)  Q=ABS(FLOAT(LLP(IK)/IL)*QI(IL)+DQ(II))
!      QQQ1=ABS(FLOAT(LLP(IK)/IL))*QI(IL)+DQ(II) ! 1
!      QQQ2=ABS(FLOAT(LLP(IK)/IL)*QI(IL))+DQ(II) ! 2
!      QQQ3=ABS(FLOAT(LLP(IK)/IL)*QI(IL)+DQ(II)) ! 3
!      Q = QQQ3
      Q=ABS(FLOAT(LLP(IK)/IL)*QI(IL)+DQ(II)) ! 3
!      write(6, "('Q variants: 1) ', ES12.4, ', 2) ', ES12.4, ', 3) ',"  &
!            // "ES12.4, ', setting Q = ', ES12.4)") QQQ1, QQQ2, QQQ3, Q
!      HPPP1=(A(IK)*QQQ1+B(IK))*QQQ1+HO(IK)
!      HPPP2=(A(IK)*QQQ2+B(IK))*QQQ2+HO(IK)
!      HPPP3=(A(IK)*QQQ3+B(IK))*QQQ3+HO(IK)
      HP=(A(IK)*Q+B(IK))*Q+HO(IK)
!      write(6, "('HP variants: 1) ', ES12.4, ', 2) ', ES12.4, ', 3) ',"  &
!            // "ES12.4, ', setting HP = ', ES12.4)")                     &
!            HPPP1, HPPP2, HPPP3, HP
! Out of bounds on LLP (loop containing a pump); possible typo
!      IF(LLP(II) .LT. 0) GO TO 35
      IF(LLP(IK) .LT. 0) GO TO 35
      DR(II,NLP)=DR(II,NLP)-HP
      DR(II,IL)=DR(II,IL)+2. * A(IK)*Q+B(IK)
      GO TO 333
35    CONTINUE
      DR(II,NLP)=DR(II,NLP)+HP
      DR(II,IL)=DR(II,IL)-2. * A(IK)*Q-B(IK)
333   CONTINUE
233   CONTINUE
133   CONTINUE
40    CONTINUE
      V(1)=4.
! System subroutine from UNIVAC MATH-PACK to solve linear system of
! equations
! Sperry alternate return point syntax not supported
! Type mismatch between D (real) and JC (integer); GJR expects int array
!      CALL GJR(DR,51,50,NL,NLP,$98,D,V)
      CALL GJR(DR,51,50,NL,NLP,98,JC,V)
      IF (JC(1) .NE. NL) THEN
          WRITE(6, "('Matrix failure:', /,"                             &
              // "'JC(1): ', I3, ' <> NL: ', I3)") JC(1), NL
          GOTO 98
      END IF
      DO 7 I=1,NL
      SSUM=SSUM+ABS(DR(I,NLP))
      DQ(I)=DQ(I)-DR(I,NLP)
7     CONTINUE
      NCT=NCT+1
      WRITE(6,202) NCT,SSUM,(DQ(I),I=1,NL)
202   FORMAT(' NCT=', I2, (12F10.3))
      IF(SSUM .GT.  .001 .AND. NCT .LT. MAXITER) GO TO 10
      IF(NCT.EQ. MAXITER) WRITE(6,102) NCT,SSUM
102   FORMAT(' DID NOT CONVERGE --NCT=', I5, ' SUM=', 5E12.5)
      DO 8 I=1,NL
      NNP=NN(I)
      DO 88 J=1,NNP
      IJ=LP(I,J)
      IIJ=ABS(IJ)
      QI(IIJ)=QI(IIJ)+FLOAT(IJ/IIJ)*DQ(I)
88    CONTINUE
8     CONTINUE
      WRITE(6,105) (QI(I),I=1,NP)
105   FORMAT(' FLOWRATES IN PIPES', /, 13(1X, F10.3))
      DO 9 I=1,NP
      D(I)=K(I)* ABS(QI(I))**1.852
9     CONTINUE
      WRITE(6,106) (D(I),I=1 ,NP)
106   FORMAT(' HEAD LOSSES IN PIPES', /, 13(1X, F10.3))
      GO TO 98
99    STOP
      END PROGRAM JEPPSON_CH6B
