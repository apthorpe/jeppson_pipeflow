# Building the Software

## Overview

The Jeppson Darcy_Weisbach fraction factor calculator uses CMake as its build
system. The general method of building software with CMake is to unpack the
source archive, create a separate build directory outside the source tree, run
CMake to populate the build directory and configure tools, then run `make` in
the build directory to compile and link the code. The executable will be
located in the `src` folder within the build directory.

Note that if CMake is not available, a Makefile is provided in the `src`
directory of the original source archive. It may be necessary to modify this
file to use the desired compiler and compile/link options.

## Prerequisites

 * A modern Fortran compiler (tested with gfortran 5.2.0 and Intel ifort 13.1.0
   on Linux)
 * CMake (at least version 2.8.7), available at https://cmake.org/

To build the documentation, *doxygen* (http://www.stack.nl/~dimitri/doxygen/)
is necessary. If PDF documentation is desired, a working *LaTeX* installation
such as TeXLive (https://www.tug.org/texlive/) is required. Neither are
strictly required to build and run JEPPSON_CH2.

## Unpack the Source Archive

Unzip this archive

    unzip -r JEPPSON_CH2.zip

(files unzip to ./JEPPSON_CH2)

## Create the Build Environment

Create a build directory and move to it:

    mkdir ./build
    cd ./build

Execute cmake setting the specific fortran compiler with
`CMAKE_Fortran_COMPILER` (`gfortran` or `ifort`) and setting the build type
with `CMAKE_BUILD_TYPE` (`DEBUG` or `RELEASE`) Remember to specify the base
directory of this archive as an argument (`../JEPPSON_CH2`)

    cmake -D CMAKE_Fortran_COMPILER=/home/apthorpe/bin/gcc/5.2.0/bin/gfortran \
          -D CMAKE_BUILD_TYPE=RELEASE  ../JEPPSON_CH2 

## Build the Software and Documentation

Build the software using `make` or equivalent build tool

    make

The executable is located at `./src/JEPPSON_CH2`

Build the documentation with

    make doc

To build the documentation directly, run

    doxygen Doxyfile

from the root of the original archive directory.

To generate PDF documentation via *LaTeX*,

    cd ./doc/latex 
    make 
    cd ../..

and the results will be in `./doc/latex/refman.pdf` beneath the build
directory.

## Test the Software

Test cases and reference data are located in the source archive under
`JEPPSON_CH2/test`. Four input cases are provided which reflect calculations in
the text: `test10.inp`, `test11.inp`, `test20.inp`, and `test21.inp`. Cases may
be run as:

    ./src/JEPPSON_CH2 < ../JEPPSON_CH2/test/test1/test10.inp | tee test10.out

Compare to the reference results with

    diff test10.out ../JEPPSON_CH2/test/ref/test10.out

These results are consistent with ES and SI results published include
(Jeppson, 1976)

## To Do

This process should be expanded to include setting up a generator for Visual
Studio and the Intel compiler on Windows.
