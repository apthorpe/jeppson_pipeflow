# README #

`JEPPSON_CH2` calculates the Darcy-Weisback friction factor and head loss as
described on pages 20 of (Jeppson, 1974) and pages 39 of (Jeppson, 1976)

Several errors have been corrected in the original published code (typo in
final format statement and in the calculation of head loss).

Build instructions have been provided for Linux using both gfortran and the
Intel ifort compiler. The code and documentation can be built using the CMake
cross-platform build system. A simple Makefile is provided in the `src`
directory for building the JEPPSON_CH2 executable if CMake is not available.

Several test cases are provided under the `test` directory; `test1` contains
the input and output for the example case given in (Jeppson, 1976) in both ES
and SI units.

## License ##

No license was provided with the original source code as published in either
(Jeppson, 1974) paper or (Jeppson, 1976). All other code provided under the MIT
(`expat`) license as written in `LICENSE.md`

## Who do I talk to? ##

Questions or comments can be directed to the project maintainer,
<bob.apthorpe@gmail.com>

## References ##

* Jeppson, Roland W. *Steady Flow Analysis of Pipe Networks: An Instructional
  Manual* (1974). *Reports.* Paper 300.
  http://digitalcommons.usu.edu/water_rep/300 
* Jeppson, Roland W., *Analysis of Flow in Pipe Networks* (1976). Ann Arbor
  Science Publishers, Inc.
  http://www.worldcat.org/title/analysis-of-flow-in-pipe-networks/oclc/927534147
