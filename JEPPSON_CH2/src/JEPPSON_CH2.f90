!> Darcy-Weisback friction factor solver
      PROGRAM JEPPSON_CH2
10    READ(5, 100, END=99) D, Q, FL, VIS, E, G
! D-pipe diameter, Q-flow rate, FL-length of pipe
! VIS-kinematic viscosity of fluid x 10**5,
! E-absolute roughness of pipe, G-acceleration of gravity
100   FORMAT(8F10.5)
      A = 0.78539816*D*D
      V = Q/A
      RE = V*D/VIS
      IF (RE .GT. 2100) GO TO 3
      F = 64./RE
      GO TO 1
3     CONTINUE
      EVIS = E/VIS
      ELOG = 9.35*LOG10(2.71828183)
      ED = E/D
      F = 1./(1.14-2.*LOG10(ED))**2
      PAR = V*SQRT(F/8.)*EVIS
      IF (PAR .GT. 100.) GO TO 1
      NCT = 0
2     CONTINUE
      FS = SQRT(F)
      FZ = .5/(F*FS)
      ARG = ED + 9.35/(RE*FS)
      FF = 1./FS -1.14 + 2.*LOG10(ARG)
      DF = FZ + ELOG*FZ/(ARG*RE)
      DIF = FF/DF
      F = F+DIF
      NCT = NCT + 1
      IF (ABS(DIF) .GT.  .00001 .AND. NCT .LT. 15) GO TO 2
1     CONTINUE
      HL = F*FL*V*V/(2.*G*D)
      WRITE (6,101) Q,D,FL,F,HL
101   FORMAT ('Q=',F10.4,'D=',F10.4, 'L=',F10.2,'F=',F10.5,             &
      'HEADLOSS=', F10.4)
      GO TO 10
99    STOP
      END PROGRAM JEPPSON_CH2
