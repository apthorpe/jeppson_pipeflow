# Building the Software

## Overview

The Jeppson code suite uses CMake as its build system. The general method of
building software with CMake is to unpack the source archive, create a separate
build directory outside the source tree, run CMake to populate the build
directory and configure tools, then run `make` in the build directory to
compile and link the code. The executable for each code will be located in the
`src` folder within each JEPPSON_CH* build directory.

Note that if CMake is not available, a Makefile is provided in the `src`
directory in each of the JEPPSON_CH* project directories. It may be necessary
to modify this file to use the desired compiler and compile/link options.

## Prerequisites

 * A modern Fortran compiler (tested with gfortran 5.2.0 and Intel ifort 13.1.0
   on Linux)
 * LAPACK linear algebra library, available at http://www.netlib.org/lapack/
 * CMake (at least version 2.8.7), available at https://cmake.org/

LAPACK is often supplied as prepackaged vendor library with Linux distributions
or as part of the Intel Math Kernel Library (MKL)

To build the documentation, *doxygen* (http://www.stack.nl/~dimitri/doxygen/)
is necessary. If PDF documentation is desired, a working *LaTeX* installation
such as TeXLive (https://www.tug.org/texlive/) is required. Neither are
strictly required to build and run the codes.

## Unpack the Source Archive

Unzip this archive

    unzip -r jeppson.zip

(files unzip to ./jeppson)

## Create the Build Environment

Create a build directory and move to it:

    mkdir ./build
    cd ./build

Execute cmake setting the specific fortran compiler with
`CMAKE_Fortran_COMPILER` (`gfortran` or `ifort`) and setting the build type
with `CMAKE_BUILD_TYPE` (`DEBUG` or `RELEASE`) Remember to specify the base
directory of this archive as an argument (`../jeppson`)

    cmake -D CMAKE_Fortran_COMPILER=/home/apthorpe/bin/gcc/5.2.0/bin/gfortran \
          -D CMAKE_BUILD_TYPE=RELEASE  ../jeppson

## Build the Software and Documentation

Build the software using `make` or equivalent build tool

    make

The executables are located at `jeppson/JEPPSON_CH*/src/JEPPSON_CH*`

Build the documentation with

    make doc

To build the documentation directly, run

    doxygen Doxyfile

from the root of the original archive directory.

To generate PDF documentation for a project via *LaTeX*,

    cd ./JEPPSON_CH*/doc/latex 
    make 
    cd ../..

and the results will be in `./JEPPSON_CH*/doc/latex/refman.pdf` beneath the
build directory.

## Test the Software

See the README.md in each project directory for testing instructions.

## To Do

This process should be expanded to include setting up a generator for Visual
Studio and the Intel compiler on Windows.
