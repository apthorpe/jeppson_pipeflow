# README #

This project provides verified, modernized versions of the programs described
in the piping network analysis text by Roland W. Jeppson (Jeppson, 1976) and
(Jeppson, 1974). 

`JEPPSON_CH2` calculates the Darcy-Weisback friction factor and head loss as
described on pages 20 of (Jeppson, 1974) and pages 39 of (Jeppson, 1976).

`JEPPSON_CH4` is an incompressible flow pipe network solver as described on
page 34 of (Jeppson, 1974) and pages 64-65 of (Jeppson, 1976).

`JEPPSON_CH5` is a piping network flow solver based on the linear method solver
code described on pages 41-42 of (Jeppson, 1974) and pages 75-58 of 
(Jeppson, 1976).

`JEPPSON_CH6A` is a Newton-Raphson method piping network flow solver based on
the Newton-Raphson solver code described on pages 63 of (Jeppson, 1974) and
pages 119-121 of (Jeppson, 1976).

`JEPPSON_CH6B` is a piping network flow solver based on the corrective flow
Newton-Raphson solver code described on pages 66-67 of (Jeppson, 1974) and
pages 126-128 of (Jeppson, 1976).

`JEPPSON_CH7` solves pipe flow networks using the Hardy Cross method as
described on page 75 of (Jeppson, 1974) and pages 148-150 of (Jeppson, 1976).

Sample input and reference output has been provided for each program, allowing
the codes to be benchmarked against sample calculations presented in the text.
During the verification process, a number of errors in the original sample
problems were found and are noted in the `README.md` of individual codes.

Build instructions have been provided for Linux using both gfortran and the
Intel ifort compiler. The code and documentation can be built using the CMake
cross-platform build system. A simple Makefile is provided in the `src`
directory of each project (e.g. `JEPPSON_CH2`) for building the executable if 
CMake is not available. Each program may be built separately (see `BUILD.md` in
each project directory) or as a group.

The original software was written in 'FORTRAN 5', a proprietary dialect of
FORTRAN 66 distributed by Sperry-Rand for their UNIVAC series of computers. A
number of errors in the original programs have been corrected and
UNIVAC-specific routines and coding has been slightly altered to make the
programs platform-independent and compatible with modern Fortran compilers
(Fortran 90 and later). The proprietary Sperry-Rand MATH-PACK routine
`GJR()` has been replaced with a compatibility routine which uses the
freely-available LAPACK linear algebra library.

These codes are provided as an educational resource and no warranty is made to
their accuracy or suitability for engineering use, especially in any use
involving protection of human life and environmental quality. *Caveat utilitor!*

## Refactoring Phases ##

The code repository may be used as an illustrative example of legacy Fortran
revitalization. Several named branches have been created, each associated with
a particular refactoring effort.

### Phase 0: Resuscitate ###

The *phase0* branch contains minimal edits to the original source to arrive at
clean compiling, tested, working code. Some formatting changes were made to
comply with the Fortran 90 standard and modern features were used to define and
initialize arrays but little effort was put into modernizing the code; it very
much resembles the original FORTRAN 66 (UNIVAC 'FORTRAN 5') source code as
published by Jeppson.

### Phase 1: Update ###

The *phase1* branch contains indented, documented, free-format Fortran, with
much of the `GOTO` logic replaced with modern control structures such as
`DO/WHILE`, `CYCLE`, and `EXIT`. Some numeric literals are replaced with named
constants, and all variables were forced to be declared by setting 
`IMPLICIT NONE`. The result of this refactoring is clearer code, ready for more
complex refactoring - modularization, parallelization, *etc.*

### Phase 2: Modularize ###

The *phase2* branch adds the `m_jeppson_friction` module which contains a number
of pipe flow friction correlations extracted from the applications. This acts as
a shared dependency and greatly clarifies the individual applications. Numerical
precision may be parameterized in this phase, allowing 64-bit (double) *vs*
32-bit (single) precision results to be easily compared. 

## License ##

No license was provided with the original source code as published in either
(Jeppson, 1974) paper or (Jeppson, 1976). All other code provided under the MIT
(`expat`) license as written in `LICENSE.md`

## Who do I talk to? ##

Questions or comments can be directed to the project maintainer,
<bob.apthorpe@gmail.com>

## References ##

* Jeppson, Roland W. *Steady Flow Analysis of Pipe Networks: An Instructional
  Manual* (1974). *Reports.* Paper 300.
  http://digitalcommons.usu.edu/water_rep/300 
* Jeppson, Roland W., *Analysis of Flow in Pipe Networks* (1976). Ann Arbor
  Science Publishers, Inc. 
  http://www.worldcat.org/title/analysis-of-flow-in-pipe-networks/oclc/927534147
