!> Hardy-Cross pipe network solver
!! Modifications include minimal changes to comply with F90 free-format,
!! typographical error fixes, decoupling statement labels from
!! executable statements (labels applied only to FORMAT and CONTINUE
!! statements), and adjustment of read format specifiers.
!! Alternative which calculates loop corrections independently and
!! applies them simultaneously. Requires more memory and may take longer
!! to converge.
      PROGRAM JEPPSON_CH7A
      REAL D(50),L(50),K(50),E(50),QI(50),DQ(42)
      INTEGER LP(42,7),NN(42)
! NP-NO. OF PIPES, NL-NO. OF LOOPS, MAXITER-MAX. NO.
! OF ITERATIONS ALLOWED.
! VIS- VISCo OF FLUID, ERR-ERROR PARAMETER, G2-
! 2 X ACCEL OF GRAVITY
! Fixed error in READ() statement and decoupled line label
!98      READ(5,100),END=99) NP,NL,MAXITER,VIS,ERR,G2
98    CONTINUE
      READ(5,100,END=99) NP,NL,MAXITER,VIS,ERR,G2
100   FORMAT(3I5,3F10.5)
! II-PIPE NO., D(lO-DIAMETER OF PIPE IN INCHES, L(II)-
! LENGTH OF PIPE IN FT, E(U)-WALL ROUGHNESS OF
! PIPE, QI(II)-INITIAL FLOW SATISFYING CONTINUITY
! EQS.
      DO 1 I=1,NP
      READ(5, 101) II,D(II),L(II),E(II),QI(II)
      E(II)=E(II)/D(II)
      D(II)=D(II)/12.
1     CONTINUE
101   FORMAT(I5,4F10.5)
      IF(G2 .LT. 1.E-5) G2=64.4
      ELOG=9.35*LOG10(2.71828183)
      DO 51 I=1,NP
      QM=ABS(QI(I))
      DEQ=.1*QM
      AR=.78539392*D(I)**2
      ARL=L(I)/ (G2*D(I)*AR**2)
      V1=(QM-DEQ)/ AR
      V2=(QM+DEQ)/ AR
      RE1=V1*D(I)/VIS
      RE2=V2*D(I)/VIS
      IF(RE2 .GT. 2.1E3) GO TO 53
      F1=64./RE1
      F2=64./RE2
      E(I)=1.
      K(I)=64.4*VIS*ARL/D(I)
      GOTO 51
53    CONTINUE
      MM=0
      F= 1./ (1.14-2.*LOG10(E(I)))**2
      PAR=V1*SQRT(.125*F)*D(I)*E(I)/VIS
      IF(PAR .GT. 65.) GO TO 54
      RE=RE1
57    CONTINUE
      MCT=0
52    CONTINUE
      FS=SQRT(F)
      FZ=.5/(F*FS)
      ARG=E(I)+9.35/(RE*FS)
      FF=1./FS-1.14+2.*LOG10(ARG)
      DF=FZ+ELOG*FZ/(ARG*RE)
      DIF=FF/DF
      F=F+DIF
      MCT=MCT+1
      IF(ABS(DIF) .GT. .00001 .AND. MCT .LT. 15) GO TO 52
      IF(MM .EQ. 1) GO TO 55
      MM=1
      RE=RE2
      F1=F
      GO TO 57
55    CONTINUE
      F2=F
      BE=(LOG(F1)-LOG(F2))/(LOG(QM+DEQ)-LOG(QM-DEQ))
      AE=F1*(QM-DEQ)**BE
      E(i)=2.-BE
      K(I)=AE*ARL
      GO TO 51
54    CONTINUE
      K(I)=F*ARL
      E(I)=2.
51    CONTINUE
      WRITE(6,110) (K(I),I=1,NP)
110   FORMAT('O COEF. K IN EXPONENTIAL FORMULA', /, (1X, 16F8.4))
      WRITE(6,111) (E(I),I=1,NP)
111   FORMAT('O EXPONENT N IN EXPONENTIAL FORMULA', /, (1X, 16F8.4))
      DO 2 I=1,NL
! NNP IS THE NUMBER OF PIPES AROUND THE LOOP
! LP(I,J) ARE THE PIPE NO. AROUND THE LOOP. IF
! COUNTERCLOCKWISE THIS NO. IS NEGATIVE
      READ(5,120) NNP,(LP(I,J),J=1,NNP)
120   FORMAT(16I5)
      NN(I)=NNP
2     CONTINUE
      NCT=0
10    CONTINUE
!      write(6,"(/,'Iteration: ', I3)") NCT+1
      SSUM=0.
      DO 3 I=1,NL
      NNP=NN(I)
      SUM1=0.
      SUM2=0.
      DO 4 J=1,NNP
      IJ=LP(I,J)
      IIJ=ABS(IJ)
      HL=FLOAT(IJ/IIJ)*K(IIJ)*QI(IIJ)**E(IIJ)
      SUM1=SUM1+HL
      SUM2=SUM2+E(IIJ)*ABS(HL)/QI(IIJ)
!      write(6,"('Loop ', I3, ', Segment ', I3, ' = Pipe ', I3,"         &
!           // "', K = ', ES12.4, ', QI = ', ES12.4, ', E = ',"          &
!           // "ES12.4, ', HL = ', ES12.4)")                             &
!              I, J, IJ, K(IIJ), QI(IIJ), E(IIJ), HL
4     CONTINUE
      DQ(I)=SUM1/SUM2
      SSUM=SSUM+ABS(DQ(I))
!      write(6,"('Loop DQ = ', ES12.4, ' = ', ES12.4,"                   &
!          // "' / ', ES12.4, /)") DQ(I), SUM1, SUM2
3     CONTINUE
      DO 43 I=1,NL
      DO 33 J=1,NNP
      IJ=LP(I,J)
      IIJ=ABS(IJ)
      QI(IIJ)=QI(IIJ)-FLOAT(IJ/IIJ)*DQ(I)
33    CONTINUE
43    CONTINUE
      NCT=NCT+1
      IF(NCT .LT. MAXITER .AND. SSUM .GT. ERR) GO TO 10
      WRITE(6,105) (QI(I),I=1,NP)
105   FORMAT('FLOWRATES IN PIPES',/,(1X, 13F10.3))
      DO 9 I=1,NP
      D(I)=K(I)* ABS(QI(I))**E(I)
9     CONTINUE
      WRITE(6,106) (D(I),I=1,NP)
106   FORMAT(' HEAD LOSSES IN PIPES',/,(1X ,13F10.3))
      GO TO 98
99    CONTINUE
      STOP
      END PROGRAM JEPPSON_CH7A
