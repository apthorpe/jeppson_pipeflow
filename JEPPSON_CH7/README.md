# README #

`JEPPSON_CH7` solves pipe flow networks using the Hardy-Cross method as
described on page 75 of (Jeppson, 1974) and pages XX-XX of (Jeppson, 1976).

Build instructions have been provided for Linux using both gfortran and the
Intel ifort compiler. The code and documentation can be built using the CMake
cross-platform build system. A simple Makefile is provided in the `src`
directory for building the JEPPSON_CH7 executable if CMake is not available.

The `contrib` directory contains a modified version which calculates flow
corrections independently for each loop on each iteration and applies the sum
of all corrections at the end of the iteration. This takes more memory and may
increase the number of iterations taken to converge. The benefit of this
approach is that flow corrections may be calculated in parallel for each loop
which can significantly reduce execution time on parallel hardware. The
application needs to be restructured to take advantage of parallel features in
modern Fortran and this provides a starting point for further research.

A test case is provided under the `test` directory; `test1` contains the input
and output for the example case given in (Jeppson, 1976). The solutions are 
similar but the reference results presented with this test case are more
accurate, likely due to limitations in manual calculation methods used in the
source text (e.g., limited precision of slide rules).

## License ##

No license was provided with the original source code as published in either
(Jeppson, 1974) paper or (Jeppson, 1976). All other code provided under the MIT
(`expat`) license as written in `LICENSE.md`

## Who do I talk to? ##

Questions or comments can be directed to the project maintainer,
<bob.apthorpe@gmail.com>

## References ##

* Jeppson, Roland W. *Steady Flow Analysis of Pipe Networks: An Instructional
  Manual* (1974). *Reports.* Paper 300.
  http://digitalcommons.usu.edu/water_rep/300 
* Jeppson, Roland W., *Analysis of Flow in Pipe Networks* (1976). Ann Arbor
  Science Publishers, Inc.
  http://www.worldcat.org/title/analysis-of-flow-in-pipe-networks/oclc/927534147
